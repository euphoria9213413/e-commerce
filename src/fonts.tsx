import { Global } from "@emotion/react";

export const GlobalFonts = () => (
  <Global
    styles={`
      @font-face {
        font-family: 'Inter';
        font-style: normal;
        font-weight: 200;
        src: local(''), url('/fonts/Inter-VariableFont_slnt,wght.woff2') format('woff2');
        font-display: swap;
      }
      @font-face {
        font-family: 'Inter';
        font-style: normal;
        font-weight: 300;
        src: local(''), url('/fonts/Inter-VariableFont_slnt,wght.woff2') format('woff2');
        font-display: swap;
      }
      @font-face {
        font-family: 'Inter';
        font-style: normal;
        font-weight: 400;
        src: local(''), url('/fonts/Inter-VariableFont_slnt,wght.woff2') format('woff2');
        font-display: swap;
      }
      @font-face {
        font-family: 'Inter';
        font-style: normal;
        font-weight: 500;
        src: local(''), url('/fonts/Inter-VariableFont_slnt,wght.woff2') format('woff2');
        font-display: swap;
      }
      @font-face {
        font-family: 'Inter';
        font-style: normal;
        font-weight: 600;
        src: local(''), url('/fonts/Inter-VariableFont_slnt,wght.woff2') format('woff2');
        font-display: swap;
      }
      @font-face {
        font-family: 'Inter';
        font-style: normal;
        font-weight: 700;
        src: local(''), url('/fonts/Inter-VariableFont_slnt,wght.woff2') format('woff2');
        font-display: swap;
      }
      @font-face {
        font-family: 'Inter';
        font-style: normal;
        font-weight: 900;
        src: local(''), url('/fonts/Inter-VariableFont_slnt,wght.woff2') format('woff2');
        font-display: swap;
      }
      @font-face {
        font-family: 'Manrope';
        font-style: normal;
        font-weight: 700;
        src: local(''), url('/fonts/Manrope-VariableFont_wght.woff2') format('woff2');
        font-display: swap;
      }
      @font-face {
        font-family: 'Lato';
        font-style: normal;
        font-weight: 400;
        src: local(''), url('/fonts/Lato-Regular.woff2') format('woff2');
        font-display: swap;
      }
      @font-face {
        font-family: 'Lato';
        font-style: normal;
        font-weight: 500;
        src: local(''), url('/fonts/Lato-Regular.woff2') format('woff2');
        font-display: swap;
      }
    `}
  />
);
