import { LocalStorageEnum } from "@/utils/localStorage";
import axios, { AxiosError, InternalAxiosRequestConfig } from "axios";
import { env } from "./env";
import { LoginAuthenticated } from "@/pages/login/types";

export const headers = {
  Accept: "application/json",
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Headers": "*",
  "Content-Type": "application/json",
};

export const api = axios.create({
  baseURL: env.api_uri,
  timeout: 180000,
  headers,
});

const addTokenInRequest = async (config: InternalAxiosRequestConfig) => {
  const authentication = localStorage.getItem(LocalStorageEnum.ACCESS_TOKEN);

  if (config?.headers && authentication) {
    const authenticationParsed = JSON.parse(
      authentication
    ) as LoginAuthenticated;
    config.headers[
      "Authorization"
    ] = `Bearer ${authenticationParsed.accessToken}`;
  }
  return config;
};

api.interceptors.request.use(addTokenInRequest);

const redirectIfUnderMaintenance = (statusCode?: number) => {
  if (statusCode === 401) {
    localStorage.removeItem(LocalStorageEnum.ACCESS_TOKEN);

    if (window.location.pathname !== "/login") {
      return (window.location.href = env.main_uri + "/login");
    }
  }
};

api.interceptors.response.use(
  (response) => response,
  function (error: Error | AxiosError) {
    if (axios.isAxiosError(error)) {
      redirectIfUnderMaintenance(error.response?.status);
    }
    return Promise.reject(error);
  }
);
