export const env = {
  main_uri: process.env.REACT_APP_MAIN_URI,
  api_uri: process.env.REACT_APP_API_URI,
  per_page: Number(process.env.REACT_APP_PER_PAGE),
  payment_secret: process.env.REACT_APP_PAYMENT_SECRET,
};
