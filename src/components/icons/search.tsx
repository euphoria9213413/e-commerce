import { Center, CenterProps } from "@chakra-ui/react";

export const SearchIcon = ({ ...rest }: CenterProps) => {
  return (
    <Center color="#F15A22" {...rest}>
      <svg
        width="18"
        height="17"
        viewBox="0 0 18 17"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M16.3125 16.0488L12.1339 11.8702M13.5268 7.3435C13.5268 10.6128 10.8764 13.2632 7.60708 13.2632C4.33774 13.2632 1.68741 10.6128 1.68741 7.3435C1.68741 4.07416 4.33774 1.42383 7.60708 1.42383C10.8764 1.42383 13.5268 4.07416 13.5268 7.3435Z"
          stroke="currentColor"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </Center>
  );
};
