import { Center, CenterProps } from "@chakra-ui/react";

export const PaymentCheckIcon = ({ ...rest }: CenterProps) => {
  return (
    <Center color="#6BB70B" {...rest}>
      <svg
        width="208"
        height="147"
        viewBox="0 0 208 147"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M200.455 7L67.4545 140L7 79.5455"
          stroke="currentColor"
          strokeWidth="13.354"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </Center>
  );
};
