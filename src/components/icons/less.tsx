import { Center, CenterProps } from "@chakra-ui/react";

export const LessIcon = ({ ...rest }: CenterProps) => {
  return (
    <Center color="#7C8096" {...rest}>
      <svg
        width="10"
        height="3"
        viewBox="0 0 10 3"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M1.71851 1.70459H8.78647"
          stroke="currentColor"
          strokeWidth="1.67961"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </Center>
  );
};
