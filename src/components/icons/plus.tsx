import { Center, CenterProps } from "@chakra-ui/react";

export const PlusIcon = ({ ...rest }: CenterProps) => {
  return (
    <Center {...rest}>
      <svg
        width="26"
        height="26"
        viewBox="0 0 26 26"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect width="26" height="26" fill="url(#pattern0)" />
        <defs>
          <pattern
            id="pattern0"
            patternContentUnits="objectBoundingBox"
            width="1"
            height="1"
          >
            <use xlinkHref="#image0_567_2007" transform="scale(0.015625)" />
          </pattern>
          <image
            id="image0_567_2007"
            width="64"
            height="64"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABAEAQAAABQ8GUWAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAACYktHRAAAqo0jMgAAAAlwSFlzAAAAYAAAAGAA8GtCzwAAAAd0SU1FB+cHEhECIJsZSZMAAAEBSURBVHja7dqxDYJAGMXxwzjGDeACJtwe0DoBFaUDWFI5gS3scSQswADsga1ECv2IPJP7/8pLgMdLjuS+4BwAAAAAJChTPPR+rmvnQliuxlgNTbN3lqOiAOdCmC9F8bqSPTRJDprH/g8KUAdQowB1ADUKUAdQowB1ADUKUAdQowB1ALXkCzDNA9bP85+bb3nuRu8Xi6dpyq59b38V2zzBOA94P89/ZVxb837LPa3zhOS3AAWoA6gZvwExbpnh/eojaLlKNBVu2/ehaNdVQ1nunSX5LUAB6gBqFKAOoEYB6gBqFKAOoEYB6gBqFKAOoJZ8AaJ/hNbmCbbzPAAAAADA4AliPTPxQKgUKQAAAABJRU5ErkJggg=="
          />
        </defs>
      </svg>
    </Center>
  );
};
