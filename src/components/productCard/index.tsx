import { Box, Button, HStack, Image, Text } from "@chakra-ui/react";
import { DayMonthBox } from "../dayMonthBox";
import { useNavigate } from "react-router-dom";
import { StoreEventDto } from "@/pages/home/types";
import { dateStringToShortMonthUtc, getDate, getHour } from "@/utils/dateUtil";
import { moneyFormat } from "@/utils/formatUtil";

type Props = Omit<StoreEventDto, "id" | "mobileMediaUrl">;

export const ProductCard = ({
  desktopMediaUrl,
  name,
  slug,
  raffleDate,
  initialDate,
  finalDate,
  products,
}: Props) => {
  const navigate = useNavigate();

  return (
    <Box w="100%" bgColor="neutral.0" borderRadius={5} borderColor="black">
      <Image src={desktopMediaUrl} borderTopRadius={5}></Image>
      <HStack justifyContent="space-between" alignItems="flex-start" p={3}>
        <Box>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="700"
            fontSize={{ base: "17px", md: "18px" }}
            lineHeight="30px"
            color="secondary.500"
          >
            {name}
          </Text>
          <HStack
            pl={1}
            fontSize={{ base: "11px", md: "12px" }}
            fontFamily="Inter"
            fontStyle="normal"
            color="neutral.350"
            lineHeight="30px"
          >
            <Text fontWeight="400">{"Data do sorteio: "}</Text>
            <Text fontWeight="600">
              {raffleDate ? getDate(raffleDate) : ""}
            </Text>
            <Text fontWeight="400">{"às "}</Text>
            <Text fontWeight="600">
              {raffleDate ? getHour(raffleDate) : ""}
            </Text>
          </HStack>
          <HStack
            pl={1}
            fontSize={{ base: "11px", md: "12px" }}
            fontFamily="Inter"
            fontStyle="normal"
            color="neutral.350"
            lineHeight="30px"
          >
            <Text fontWeight="400">{"Período de vendas: "}</Text>
            <Text fontWeight="600">{getDate(initialDate)}</Text>
            <Text fontWeight="400">{"à "}</Text>
            <Text fontWeight="600">{getDate(finalDate)}</Text>
          </HStack>
        </Box>
        {raffleDate && (
          <DayMonthBox
            day={Number(getDate(raffleDate).split("/")[0])}
            month={dateStringToShortMonthUtc(raffleDate)}
          />
        )}
      </HStack>
      <Text
        pl={3}
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="700"
        fontSize={{ base: "14px", md: "15px" }}
        lineHeight="30px"
        color="secondary.500"
      >
        Produto
      </Text>
      {products.map((product) => (
        <HStack pl={4} key={product.name}>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="700"
            fontSize={{ base: "17px", md: "18px" }}
            lineHeight="30px"
            color="neutral.350"
          >
            {`${product.name} - ${moneyFormat(Number(product.unitaryValue))}`}
          </Text>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="300"
            fontSize={{ base: "13px", md: "14px" }}
            lineHeight="30px"
            color="neutral.350"
          >
            no pix ou cartão de crédito
          </Text>
        </HStack>
      ))}
      <HStack justifyContent="center" mt="10px" pb="20px">
        <Button
          w={230}
          h="35px"
          variant="ghost"
          bgColor="secondary.500"
          onClick={() => navigate(`/evento/${slug}`)}
          _hover={{
            bgColor: "secondary.300",
          }}
          _active={{
            bgColor: "secondary.500",
          }}
        >
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="500"
            fontSize={{ base: "13px", md: "14px" }}
            lineHeight="30px"
            color="neutral.200"
            textAlign="center"
          >
            Comprar
          </Text>
        </Button>
      </HStack>
    </Box>
  );
};
