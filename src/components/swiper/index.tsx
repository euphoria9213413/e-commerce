import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination } from "swiper/modules";
import { Box, Image, useBreakpointValue } from "@chakra-ui/react";

type EventImage = {
  desktopMediaUrl: string;
  mobileMediaUrl: string;
};

type Props = {
  images: EventImage[];
};

export default function SwiperSlideInfinite({ images }: Props) {
  const isMobile = useBreakpointValue({ base: true, md: false });

  return (
    <Box m={{ base: "0 5px", md: "30px 5px" }}>
      <Swiper
        slidesPerView={1}
        spaceBetween={30}
        loop={true}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Pagination, Navigation]}
        className="mySwiper"
      >
        {images.map((image, index) => (
          <SwiperSlide key={index}>
            <Image
              src={isMobile ? image.mobileMediaUrl : image.desktopMediaUrl}
              maxH={{ base: "250px", md: "472px" }}
            ></Image>
          </SwiperSlide>
        ))}
      </Swiper>
    </Box>
  );
}
