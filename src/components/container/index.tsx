import { Box, Flex } from "@chakra-ui/react";

type Props = {
  children: JSX.Element;
};

export const Container = ({ children }: Props) => {
  return (
    <Flex
      h="100%"
      w="100%"
      justifyContent="center"
      p={{ base: "0 10px", md: "0 10px", lg: "0 85px" }}
    >
      <Box maxW={{ base: "100%", md: "100%", lg: "1310px" }} w="100%">
        {children}
      </Box>
    </Flex>
  );
};
