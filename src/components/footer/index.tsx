import { Box, HStack, Image, Text } from "@chakra-ui/react";

const image = "/images/footer-company-logo.png";

export const Footer = () => {
  return (
    <HStack
      w="100%"
      h="150px"
      bgColor="black"
      justifyContent="space-between"
      p={{ base: "0 20px", md: "0 55px" }}
    >
      <Box
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="700"
        lineHeight="30px"
        color="neutral.0"
      >
        <Text fontSize={{ base: "10px", md: "12px" }}>Atendimento</Text>
        <Text fontSize={{ base: "20px", md: "25px" }}>(44) 3024-9092</Text>
        <Text fontSize={{ base: "10px", md: "12px" }}>
          Segunda à sexta 08:00 às 18:00
        </Text>
        <Text as="u" fontSize={{ base: "10px", md: "12px" }} cursor="pointer">
          Política de Privacidade e Termos de uso
        </Text>
      </Box>
      <Image src={image} w={{ base: 100, md: 120 }}></Image>
    </HStack>
  );
};
