import { Button, ButtonProps, HStack, Text, VStack } from "@chakra-ui/react";

type Props = {
  color?: string;
  onClick: () => void;
} & ButtonProps;

export const MenuActionButton = ({ color, onClick, ...rest }: Props) => {
  return (
    <HStack justifyContent="flex-end">
      <Button
        onClick={onClick}
        w={8}
        h={8}
        variant="ghost"
        _hover={{
          bgColor: "neutral.400",
        }}
        _active={{
          bgColor: "neutral.500",
        }}
        {...rest}
      >
        <VStack spacing={1} mb={3}>
          <Text
            fontWeight="700"
            fontSize="10px"
            h={1}
            color={`${color ?? "white"}`}
          >
            .
          </Text>
          <Text
            fontWeight="700"
            fontSize="10px"
            h={1}
            color={`${color ?? "white"}`}
          >
            .
          </Text>
          <Text
            fontWeight="700"
            fontSize="10px"
            h={1}
            color={`${color ?? "white"}`}
          >
            .
          </Text>
        </VStack>
      </Button>
    </HStack>
  );
};
