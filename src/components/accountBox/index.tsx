import { ReactNode } from "react";
import { Box, Stack } from "@chakra-ui/react";

type Props = {
  children: ReactNode;
};

export const AccountBox = ({ children }: Props) => {
  return (
    <Stack
      justifyContent="center"
      alignItems="center"
      p={{ base: "70px 20px 27px 20px", md: "70px 86px 27px 86px" }}
      backgroundColor="neutral.200"
      w={{ base: "100%", md: "550px" }}
      borderRadius="20px"
      position="relative"
    >
      <Box
        position="absolute"
        left={-5}
        top={-5}
        style={{ border: "10px solid white" }}
        borderRadius="50%"
        boxSize="74px"
        backgroundColor="secondary.500"
      />
      {children}
    </Stack>
  );
};
