import { useBreakpointValue } from "@chakra-ui/react";
import { DesktopHeader } from "./desktop";
import { MobileHeader } from "./mobile";

export const Header = () => {
  const isMobile = useBreakpointValue({ base: true, md: true, lg: false });

  return <>{isMobile ? <MobileHeader /> : <DesktopHeader />}</>;
};
