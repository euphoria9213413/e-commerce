import { useLogin } from "@/hooks/useLogin";
import { Button, Text, VStack } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

const menuOptions = [
  {
    title: "Página Inicial",
    link: "/",
  },
  {
    title: "Meu Perfil",
    link: "/perfil",
  },
  {
    title: "Meus Pedidos",
    link: "/pedidos",
  },
  {
    title: "Minhas Vendas",
    link: "/vendas",
  },
  {
    title: "Sobre Nós",
    link: "https://euphoriaformaturas.com.br/",
  },
  {
    title: "Euphoria 360°",
    link: "https://euphoriaformaturas.com.br/euphoria-360-2/",
  },
  {
    title: "Eventos",
    link: "https://euphoriaformaturas.com.br/eventos-4/",
  },
  {
    title: "Platinum",
    link: "https://euphoriaformaturas.com.br/platinum/",
  },
  {
    title: "Dúvidas",
    link: "https://euphoriaformaturas.com.br/duvidas/",
  },
  {
    title: "Sair",
    link: "/",
  },
];

export const Menu = () => {
  const navigate = useNavigate();
  const { removeAuthentication } = useLogin();

  return (
    <VStack
      bgColor="black"
      position="absolute"
      w="250px"
      top="60px"
      left="0"
      spacing={0}
      zIndex={1000}
    >
      {menuOptions.map((option, index) => (
        <Button
          key={option.title}
          display="flex"
          justifyContent="flex-start"
          w="100%"
          variant="ghost"
          _hover={{
            bgColor: "neutral.400",
          }}
          _active={{
            bgColor: "neutral.500",
          }}
        >
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="500"
            fontSize="16px"
            lineHeight="30px"
            color={index === menuOptions.length - 1 ? "fail.500" : "neutral.50"}
          >
            {option.link.includes("https") ? (
              <a href={option.link} target="_blank" rel="noreferrer">
                {option.title}
              </a>
            ) : (
              <a
                onClick={() => {
                  if (index === menuOptions.length - 1) {
                    removeAuthentication();
                  }
                  navigate(option.link);
                }}
              >
                {option.title}
              </a>
            )}
          </Text>
        </Button>
      ))}
    </VStack>
  );
};
