import { Button, HStack, Image, Text } from "@chakra-ui/react";
import { MobileMenuIcon } from "@/components/icons/mobileMenu";
import { useEffect, useRef, useState } from "react";
import { Menu } from "./menu";
import { useLogin } from "@/hooks/useLogin";
import { useNavigate } from "react-router-dom";

const mobileLogoImage = "/images/mobile-company-logo.png";

export const MobileHeader = () => {
  const { isAuthenticated } = useLogin();
  const navigate = useNavigate();
  const [showMenu, setShowMenu] = useState<boolean>(false);

  const containerRef = useRef<any>(null);

  useEffect(() => {
    const handleOutsideClick = (event: any) => {
      if (
        containerRef.current &&
        !containerRef.current.contains(event.target)
      ) {
        setShowMenu(false);
      }
    };

    document.addEventListener("mousedown", handleOutsideClick);

    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, []);

  useEffect(() => {
    if (!isAuthenticated) {
      setShowMenu(false);
    }
  }, [isAuthenticated]);

  return (
    <HStack
      ref={containerRef}
      h="70px"
      w="100%"
      padding="0 15px"
      justifyContent="space-between"
      bgColor="black"
      position="relative"
    >
      <MobileMenuIcon pl="10px" onClick={() => setShowMenu(!showMenu)} />
      <Image
        w="70px"
        src={mobileLogoImage}
        onClick={() => navigate("/")}
      ></Image>
      <Button
        borderRadius="25px"
        variant="ghost"
        bgColor="secondary.500"
        onClick={() => navigate(isAuthenticated ? "/perfil" : "/login")}
        _hover={{
          bgColor: "secondary.300",
        }}
        _active={{
          bgColor: "secondary.500",
        }}
      >
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="700"
          fontSize="15px"
          lineHeight="30px"
          color="neutral.0"
        >
          {isAuthenticated ? "MEU PERFIL" : "LOGIN"}
        </Text>
      </Button>
      {showMenu && <Menu />}
    </HStack>
  );
};
