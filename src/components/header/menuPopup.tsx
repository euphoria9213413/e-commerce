import { useLogin } from "@/hooks/useLogin";
import { Button, Text, VStack } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export const MenuPopUp = () => {
  const navigate = useNavigate();
  const { removeAuthentication } = useLogin();

  return (
    <VStack
      position="absolute"
      w={180}
      bgColor="black"
      top="60px"
      right={0}
      borderBottomStartRadius={5}
      alignItems="flex-start"
      zIndex={1000}
    >
      <Button
        display="flex"
        justifyContent="flex-start"
        borderRadius={0}
        w="100%"
        variant="ghost"
        onClick={() => navigate("/perfil")}
        _hover={{
          bgColor: "neutral.400",
        }}
        _active={{
          bgColor: "neutral.500",
        }}
      >
        <Text
          mt={1}
          fontFamily="Manrope"
          fontStyle="normal"
          fontWeight="700"
          fontSize="15px"
          lineHeight="30px"
          color="neutral.50"
        >
          Meu Perfil
        </Text>
      </Button>
      <Button
        display="flex"
        justifyContent="flex-start"
        borderRadius={0}
        w="100%"
        variant="ghost"
        onClick={() => navigate("/pedidos")}
        _hover={{
          bgColor: "neutral.400",
        }}
        _active={{
          bgColor: "neutral.500",
        }}
      >
        <Text
          mt={1}
          fontFamily="Manrope"
          fontStyle="normal"
          fontWeight="700"
          fontSize="15px"
          lineHeight="30px"
          color="neutral.50"
        >
          Meus Pedidos
        </Text>
      </Button>
      <Button
        display="flex"
        justifyContent="flex-start"
        borderRadius={0}
        w="100%"
        variant="ghost"
        onClick={() => navigate("/vendas")}
        _hover={{
          bgColor: "neutral.400",
        }}
        _active={{
          bgColor: "neutral.500",
        }}
      >
        <Text
          mt={1}
          fontFamily="Manrope"
          fontStyle="normal"
          fontWeight="700"
          fontSize="15px"
          lineHeight="30px"
          color="neutral.50"
        >
          Minhas Vendas
        </Text>
      </Button>
      <Button
        display="flex"
        justifyContent="flex-start"
        borderRadius={0}
        w="100%"
        variant="ghost"
        onClick={() => {
          removeAuthentication();
          navigate("/");
        }}
        _hover={{
          bgColor: "neutral.400",
        }}
        _active={{
          bgColor: "neutral.500",
        }}
      >
        <Text
          mt={1}
          fontFamily="Manrope"
          fontStyle="normal"
          fontWeight="700"
          fontSize="15px"
          lineHeight="30px"
          color="fail.500"
        >
          Sair
        </Text>
      </Button>
    </VStack>
  );
};
