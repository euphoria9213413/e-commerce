import { Button, HStack, Image, Text } from "@chakra-ui/react";
import { useLogin } from "@/hooks/useLogin";
import { useNavigate } from "react-router-dom";
import { MenuPopUp } from "../menuPopup";
import { useEffect, useRef, useState } from "react";

const desktopLogoImage = "/images/desktop-company-logo.png";

const menuOptions = [
  {
    title: "PÁGINA INICIAL",
    link: "/",
  },
  {
    title: "SOBRE NÓS",
    link: "https://euphoriaformaturas.com.br/",
  },
  {
    title: "EUPHORIA 360°",
    link: "https://euphoriaformaturas.com.br/euphoria-360-2/",
  },
  {
    title: "EVENTOS",
    link: "https://euphoriaformaturas.com.br/eventos-4/",
  },
  {
    title: "PLATINUM",
    link: "https://euphoriaformaturas.com.br/platinum/",
  },
  {
    title: "DÚVIDAS",
    link: "https://euphoriaformaturas.com.br/duvidas/",
  },
];

export const DesktopHeader = () => {
  const containerRef = useRef<any>(null);
  const { isAuthenticated } = useLogin();
  const navigate = useNavigate();
  const [showMenu, setShowMenu] = useState<boolean>(false);

  useEffect(() => {
    const handleOutsideClick = (event: any) => {
      if (
        containerRef.current &&
        !containerRef.current.contains(event.target)
      ) {
        setShowMenu(false);
      }
    };

    document.addEventListener("mousedown", handleOutsideClick);

    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, []);

  useEffect(() => {
    if (!isAuthenticated) {
      setShowMenu(false);
    }
  }, [isAuthenticated]);

  return (
    <HStack
      ref={containerRef}
      h="70px"
      w="100%"
      justifyContent="space-between"
      bgColor="black"
      padding="0 15px"
      position="relative"
    >
      <Image
        w={200}
        src={desktopLogoImage}
        cursor="pointer"
        onClick={() => navigate("/")}
      ></Image>
      <HStack alignItems="center" spacing={8}>
        {menuOptions.map((option, index) => (
          <Text
            key={option.title}
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="200"
            fontSize="16px"
            lineHeight="30px"
            color="neutral.0"
            cursor="pointer"
          >
            {index === 0 ? (
              <a onClick={() => navigate("/")}>{option.title}</a>
            ) : (
              <a href={option.link} target="_blank" rel="noreferrer">
                {option.title}
              </a>
            )}
          </Text>
        ))}
      </HStack>
      <Button
        borderRadius="25px"
        variant="ghost"
        bgColor="secondary.500"
        onClick={() => {
          if (isAuthenticated) {
            return setShowMenu(true);
          }

          navigate("/login");
        }}
        _hover={{
          bgColor: "secondary.300",
        }}
        _active={{
          bgColor: "secondary.500",
        }}
      >
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="700"
          fontSize="20px"
          lineHeight="30px"
          color="neutral.200"
          textAlign="center"
        >
          {isAuthenticated ? "MEU PERFIL" : "LOGIN"}
        </Text>
      </Button>
      {showMenu && <MenuPopUp />}
    </HStack>
  );
};
