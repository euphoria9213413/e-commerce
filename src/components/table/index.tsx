import { Tr } from "@chakra-ui/react";
import { ReactNode } from "react";

type Props = {
  children: ReactNode;
  hideBorderTop?: boolean;
};

export const Body = ({ children, hideBorderTop }: Props) => {
  return (
    <Tr
      h={{ base: "30px", md: "50%" }}
      bgColor="neutral.0"
      borderTop={hideBorderTop ? "" : "3px solid #EAEAEA"}
      borderRadius={5}
      position="relative"
    >
      {children}
    </Tr>
  );
};
