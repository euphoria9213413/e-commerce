import { Button, HStack, Text } from "@chakra-ui/react";
import { LessIcon } from "../icons/less";
import { PlusIcon } from "../icons/plus";
import { useCallback, useMemo } from "react";

type Props = {
  productName: string;
  productQuantity: { [key: string]: number };
  setProductQuantity: (value: { [key: string]: number }) => void;
};

export const QuantityCounter = ({
  productName,
  productQuantity,
  setProductQuantity,
}: Props) => {
  const onLessClick = useCallback(() => {
    setProductQuantity({
      ...productQuantity,
      [productName]: productQuantity[productName] - 1,
    });
  }, [productName, productQuantity]);

  const isLessButtonActive = useMemo(
    () => productQuantity[productName] > 0,
    [productQuantity]
  );

  const onPlusClick = useCallback(() => {
    setProductQuantity({
      ...productQuantity,
      [productName]: productQuantity[productName] + 1,
    });
  }, [productName, productQuantity]);

  return (
    <HStack gap={0}>
      <Button
        size="xs"
        h="34px"
        w="34px"
        border="solid 1px"
        borderColor="neutral.10"
        variant="ghost"
        bgColor="neutral.0"
        borderRadius={0}
        borderLeftRadius={8}
        isDisabled={!isLessButtonActive || false}
        onClick={onLessClick}
      >
        <LessIcon />
      </Button>
      <HStack
        h="34px"
        w="32px"
        bgColor="neutral.0"
        p="4px 8px"
        borderTop="solid 1px"
        borderBottom="solid 1px"
        borderTopColor="neutral.10"
        borderBottomColor="neutral.10"
        justifyContent="center"
      >
        <Text
          as="span"
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="400"
          fontSize={{ base: "12px", md: "14px" }}
          lineHeight="17px"
          color="neutral.600"
        >
          {productQuantity[productName]}
        </Text>
      </HStack>
      <Button
        size="xs"
        h="34px"
        border="solid 1px"
        borderColor="neutral.10"
        variant="ghost"
        bgColor="neutral.0"
        borderRadius={0}
        borderRightRadius={8}
        onClick={onPlusClick}
      >
        <PlusIcon w={4} />
      </Button>
    </HStack>
  );
};
