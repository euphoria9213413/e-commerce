import {
  Button,
  ListItem,
  Text,
  UnorderedList,
  VStack,
} from "@chakra-ui/react";
import { useEffect, useRef } from "react";

type Option = {
  title: string;
  onClick: () => void;
};

type Props = {
  top?: string;
  right?: string;
  options: Option[];
  setShowMenuPopUp: (value: number | undefined) => void;
};

export const MenuPopUp = ({ top, right, options, setShowMenuPopUp }: Props) => {
  const containerRef = useRef<any>(null);

  useEffect(() => {
    const handleOutsideClick = (event: any) => {
      if (
        containerRef.current &&
        !containerRef.current.contains(event.target)
      ) {
        setShowMenuPopUp(undefined);
      }
    };

    document.addEventListener("mousedown", handleOutsideClick);

    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, []);

  return (
    <VStack
      ref={containerRef}
      position="absolute"
      bgColor="neutral.0"
      top={top ?? "45px"}
      right={right ?? "0"}
      pl={3}
      borderRadius={5}
      alignItems="flex-start"
      zIndex={1000}
      border="1px solid #2F2F2F"
    >
      <UnorderedList m="10px 20px">
        {options.map((option, index) => (
          <ListItem key={index} w="100%">
            <Button
              w="100%"
              display="flex"
              justifyContent="flex-start"
              variant="ghost"
              onClick={option.onClick}
              _hover={{
                bgColor: "neutral.200",
              }}
              _active={{
                bgColor: "neutral.300",
              }}
            >
              <Text
                mt={1}
                fontFamily="Manrope"
                fontStyle="normal"
                fontWeight="700"
                fontSize="15px"
                lineHeight="30px"
                color="neutral.350"
              >
                {option.title}
              </Text>
            </Button>
          </ListItem>
        ))}
      </UnorderedList>
    </VStack>
  );
};
