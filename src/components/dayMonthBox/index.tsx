import { Text, VStack } from "@chakra-ui/react";

export type Props = {
  day: number;
  month: string;
};

export const DayMonthBox = ({ day, month }: Props) => {
  return (
    <VStack position="relative" w="45px" h="45px" bgColor="secondary.500">
      <Text
        position="absolute"
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="700"
        fontSize="20px"
        lineHeight="32px"
        color="neutral.0"
        top="-1"
      >
        {day}
      </Text>
      <Text
        position="absolute"
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="300"
        fontSize="15px"
        lineHeight="32px"
        color="neutral.0"
        top="18px"
      >
        {month}
      </Text>
    </VStack>
  );
};
