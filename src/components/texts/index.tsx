import { Text } from "@chakra-ui/react";

type Props = {
  children: string;
  color?: string;
};

export const TableTheadText = ({ children, color }: Props) => {
  return (
    <Text
      fontFamily="Inter"
      fontStyle="normal"
      fontWeight="900"
      fontSize={{ base: "11px", md: "11px" }}
      lineHeight="23px"
      color={color ?? "neutral.500"}
    >
      {children}
    </Text>
  );
};

export const TableBodyText = ({ children, color }: Props) => {
  return (
    <Text
      fontFamily="Inter"
      fontStyle="normal"
      fontWeight="400"
      fontSize={{ base: "11px", md: "14px" }}
      lineHeight="23px"
      color={color ?? "neutral.500"}
      display="inline-table"
      whiteSpace="pre-wrap"
    >
      {children}
    </Text>
  );
};

export const FormLabelText = ({ children, color }: Props) => {
  return (
    <Text
      fontFamily="Inter"
      fontStyle="normal"
      fontWeight="400"
      fontSize="14px"
      lineHeight="18px"
      color={color ?? "neutral.400"}
      mb={2}
    >
      {children}
    </Text>
  );
};

export const FormInputInformationText = ({ children, color }: Props) => {
  return (
    <Text
      fontFamily="Inter"
      fontStyle="normal"
      fontWeight="600"
      fontSize="14px"
      lineHeight="17px"
      color={color ?? "neutral.350"}
      mt={1}
    >
      {children}
    </Text>
  );
};

export const ButtonText = ({ children, color }: Props) => {
  return (
    <Text
      fontFamily="Inter"
      fontStyle="normal"
      fontWeight="600"
      fontSize="13px"
      lineHeight="18px"
      color={color ?? "neutral.0"}
    >
      {children}
    </Text>
  );
};
