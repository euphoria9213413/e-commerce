import { Box, Grid, GridItem, HStack, Text } from "@chakra-ui/react";
import { ReactNode } from "react";

type Filter = {
  field?: ReactNode;
};

type Props = {
  title: string;
  subTitle?: string;
  filters?: Filter[];
};

export const FilterBar = ({ title, subTitle, filters }: Props) => {
  return (
    <Box mt="0 !important" bgColor="neutral.300" w="100%" pl={5} pr={5}>
      <HStack mt={5} alignItems="center" justifyContent="space-between">
        <Box>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="600"
            fontSize="20px"
            lineHeight="31px"
            color="neutral.600"
          >
            {title}
          </Text>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="300"
            fontSize="18px"
            lineHeight="23px"
            color="neutral.600"
          >
            {subTitle}
          </Text>
        </Box>
      </HStack>
      <Grid templateColumns="repeat(2, 1fr)" gap={4} w="100%" paddingY={4}>
        {filters?.map((filter, index) => {
          const lgValue = filters.length > 1 ? 1 : 2;
          return (
            <GridItem key={index} colSpan={{ base: 2, md: 2, lg: lgValue }}>
              {filter.field}
            </GridItem>
          );
        })}
      </Grid>
    </Box>
  );
};
