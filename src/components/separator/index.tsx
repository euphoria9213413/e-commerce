import { Box, SpaceProps } from "@chakra-ui/react";

type Props = SpaceProps;

export const Separator = ({ ...rest }: Props) => {
  return (
    <Box w="100%" borderTop="solid 1px" borderColor="neutral.10" {...rest} />
  );
};
