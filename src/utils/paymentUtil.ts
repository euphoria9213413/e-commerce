import { roundFloat } from "./formatUtil";

export const getInstallmentValue = (
  installments: number,
  tax: number,
  amount?: number
) => {
  if (!amount) return `${installments}`;

  const calculatedAmount = amount + amount * (tax / 100);

  return `${installments}x de ${roundFloat(calculatedAmount / installments)}`;
};
