import { env } from "@/configs/env";
import CryptoJS from "crypto-js";

export const tokenGenerate = (data: string) => {
  const chaveSecreta = env.payment_secret!;
  const algoritmo = CryptoJS.AES;
  const valorCriptografado = algoritmo.encrypt(data, chaveSecreta);

  return valorCriptografado.toString();
};
