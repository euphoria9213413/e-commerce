import { ProfileTypeType } from "../enums";

export type ProfileDataDto = {
  id: number;
  type: ProfileTypeType;
  description: string;
};

export type UserStoreDto = {
  name: string;
  email: string;
  cpf: string;
  birth?: string;
  contact?: string;
  situation: boolean | number;
  profileId: number;
};

export type UserPasswordUpdateDto = {
  currentPassword: string;
  newPassword: string;
  newPasswordConfirmation: string;
};

export type UserEmailUpdateDto = {
  currentEmail: string;
  newEmail: string;
  password: string;
};

export type UserDataDto = {
  id?: number;
  name: string;
  email: string;
  cpf: string;
  birth: string;
  contact: string;
  situation: boolean;
  profile?: ProfileDataDto;
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
};
