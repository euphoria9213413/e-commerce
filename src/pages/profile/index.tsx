import { Footer } from "@/components/footer";
import { Header } from "@/components/header";
import { validateCpf } from "@/utils/validatorUtil";
import {
  Box,
  Button,
  FormControl,
  Grid,
  GridItem,
  Input,
  InputGroup,
  InputRightElement,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import Joi from "joi";
import { SubmitHandler, useForm } from "react-hook-form";
import * as Fonts from "@/components/texts";
import { useNavigate } from "react-router-dom";
import { useCustomToast } from "@/hooks/useToast";
import { ChangeEvent, useCallback, useEffect, useMemo, useState } from "react";
import { UserStoreDto } from "./types";
import { useUser } from "@/hooks/useUser";
import { joiResolver } from "@hookform/resolvers/joi";
import { PasswordFormModal } from "./PasswordFormModal";
import { EmailFormModal } from "./EmailFormModal";
import InputMask from "react-input-mask";
import { CheckIcon } from "@/components/icons/check";
import { Container } from "@/components/container";
import LoadingOverlay from "@/components/overlay";

const schema = Joi.object({
  name: Joi.string().required().messages({
    "string.empty": "Nome deve ser informado",
  }),
  email: Joi.string().required().messages({
    "string.empty": "E-mail deve ser informado",
  }),
  cpf: Joi.string()
    .required()
    .pattern(/[0-9]/)
    .custom((value, helper) => {
      const valueReplaced = value
        .replaceAll(".", "")
        .replaceAll("-", "")
        .replaceAll("_", "");

      const isValidCpf = validateCpf(valueReplaced);

      if (!isValidCpf || valueReplaced.length !== 11) {
        return helper.error("string.invalid");
      }

      return value;
    })
    .messages({
      "string.empty": "CPF deve ser informado",
      "string.invalid": "CPF inválido",
    }),
  birth: Joi.string().allow("", null),
  contact: Joi.string()
    .allow(null, "")
    .custom((value, helper) => {
      const valueReplaced = value
        .replaceAll("(", "")
        .replaceAll(")", "")
        .replaceAll(" ", "")
        .replaceAll("-", "")
        .replaceAll("_", "");

      if (valueReplaced.length !== 11) {
        return helper.error("string.invalid");
      }

      return value;
    })
    .messages({
      "string.invalid": "Telefone deve conter DDD e número com 9 dígitos",
    }),
  profileId: Joi.number().required().messages({
    "number.empty": "Perfil deve ser informado",
  }),
  situation: Joi.number().required().messages({
    "number.empty": "Situação deve ser informada",
  }),
});

const initialValue: UserStoreDto = {
  name: "",
  email: "",
  cpf: "",
  situation: 1,
  profileId: 0,
};

/* eslint-disable react/no-children-prop */
export const Profile = () => {
  const navigate = useNavigate();
  const { getUser, userUpdate } = useUser();
  const { customToast } = useCustomToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [modalType, setModalType] = useState<
    "password" | "e-mail" | undefined
  >();
  const [cpfError, setCpfError] = useState<boolean | undefined>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [defaultValues, setDefaultValues] =
    useState<UserStoreDto>(initialValue);

  const cpfWithoutMask = (contact: string) =>
    contact.replaceAll(".", "").replaceAll("-", "").replaceAll("_", "");

  const contactWithoutMask = (contact: string) =>
    contact
      .replaceAll("(", "")
      .replaceAll(")", "")
      .replaceAll(" ", "")
      .replaceAll("-", "")
      .replaceAll("_", "");

  const profileLoad = useCallback(() => {
    getUser()
      .then((response) => {
        const data = {
          ...response.data,
          profileId: response.data.profile!.id,
          situation: response.data.situation ? 1 : 0,
        };

        delete data.id;
        delete data.profile;
        delete data.createdAt;
        delete data.updatedAt;
        delete data.deletedAt;

        setDefaultValues(data);
        setValue("cpf", response.data.cpf);
        if (response.data.contact) setValue("contact", response.data.contact);
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  }, []);

  useEffect(() => {
    setIsLoading(true);
    profileLoad();
  }, []);

  const {
    register,
    handleSubmit,
    watch,
    setValue,
    clearErrors,
    formState: { errors },
  } = useForm<UserStoreDto>({
    resolver: joiResolver(schema),
  });

  useEffect(() => {
    setValue("name", defaultValues.name);
    setValue("email", defaultValues.email);
    setValue("cpf", defaultValues.cpf);
    setValue("birth", defaultValues.birth);
    if (defaultValues.contact) {
      setValue("contact", defaultValues.contact);
    }
    setValue("situation", defaultValues.situation);
    setValue("profileId", defaultValues.profileId);
  }, [defaultValues]);

  const cpf = watch("cpf");
  const contact = watch("contact");

  const onChangeCpfValue = (event: ChangeEvent<HTMLInputElement>) => {
    clearErrors("cpf");
    setCpfError(false);

    const value = cpfWithoutMask(event.target.value);

    const isValidNumberValue = /^[0-9]*$/.test(value);
    const validatedValue = isValidNumberValue ? value : cpf;

    setValue("cpf", validatedValue);
  };

  const onChangeContactValue = (event: ChangeEvent<HTMLInputElement>) => {
    clearErrors("contact");

    const value = contactWithoutMask(event.target.value);

    const isValidNumberValue = /^[0-9]*$/.test(value);
    const validatedValue = isValidNumberValue ? value : contact;

    setValue("contact", validatedValue);
  };

  const onBlurCpfValue = () => {
    if (!cpf) return;

    const value = cpfWithoutMask(cpf);
    const isValidCpf = validateCpf(value);

    if (!isValidCpf) {
      setCpfError(true);
    }
  };

  const cpfInputColor = useMemo(() => {
    if (!cpf && !cpfError) return "#EAEAEA";
    if (errors?.cpf?.message || cpfError) return "red";

    if (cpfWithoutMask(cpf).length === 11) return "#62CD0F";

    return "#EAEAEA";
  }, [cpf, errors?.cpf?.message, cpfError]);

  const onSubmit: SubmitHandler<UserStoreDto> = (props) => {
    const data: UserStoreDto = {
      ...props,
      cpf: cpfWithoutMask(props.cpf),
      contact: props.contact ? contactWithoutMask(props.contact) : "",
      birth: props.birth || undefined,
      situation: props.situation ? true : false,
    };
    setIsLoading(true);

    userUpdate(data)
      .then(() => {
        customToast("success", "Atualizado com sucesso");
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  const onModalFormClose = () => {
    onClose();
    setModalType(undefined);
  };

  return (
    <Stack
      h={{ base: "100%", md: "100%", lg: "100vh" }}
      w="100%"
      position="relative"
    >
      <Header />
      {isLoading && <LoadingOverlay />}
      <Stack w="100%" h="100%" p={5}>
        <Container>
          <Stack spacing={7}>
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="700"
              fontSize="20px"
              lineHeight="32px"
              color="secondary.500"
            >
              Meu Usuário
            </Text>
            <Box
              bgColor="neutral.100"
              borderRadius={5}
              p={3}
              as="form"
              onSubmit={handleSubmit(onSubmit)}
            >
              <Grid templateColumns="repeat(3, 1fr)" gap={5}>
                <GridItem colSpan={{ base: 3, md: 3, lg: 2 }}>
                  <FormControl w="100%">
                    <Fonts.FormLabelText>Nome Completo*</Fonts.FormLabelText>
                    <Input
                      type="text"
                      textIndent={"1px"}
                      w="100%"
                      h={10}
                      borderRadius={3}
                      border="1px solid #EAEAEA"
                      bgColor="neutral.0"
                      {...register("name")}
                    />
                    {errors?.name?.message && (
                      <Text
                        fontFamily="Lato"
                        fontStyle="normal"
                        fontWeight="500"
                        fontSize="11px"
                        lineHeight="18px"
                        color="fail.500"
                      >
                        {errors.name.message}
                      </Text>
                    )}
                  </FormControl>
                </GridItem>
                <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
                  <FormControl w="100%">
                    <Fonts.FormLabelText>E-mail*</Fonts.FormLabelText>
                    <Input
                      type="text"
                      textIndent={"1px"}
                      w="100%"
                      h={10}
                      borderRadius={3}
                      border="1px solid #EAEAEA"
                      bgColor="neutral.0"
                      isDisabled={true}
                      {...register("email")}
                    />
                    {errors?.email?.message && (
                      <Text
                        fontFamily="Lato"
                        fontStyle="normal"
                        fontWeight="500"
                        fontSize="11px"
                        lineHeight="18px"
                        color="fail.500"
                      >
                        {errors.email.message}
                      </Text>
                    )}
                  </FormControl>
                </GridItem>
                <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
                  <FormControl w="100%">
                    <Fonts.FormLabelText>CPF*</Fonts.FormLabelText>
                    <InputGroup>
                      <Input
                        {...register("cpf")}
                        as={InputMask}
                        mask="***.***.***-**"
                        type="text"
                        textIndent={"1px"}
                        w="100%"
                        h={10}
                        borderRadius={3}
                        border="1px solid #EAEAEA"
                        bgColor="neutral.0"
                        isDisabled={true}
                        borderColor={cpfInputColor}
                        onChange={onChangeCpfValue}
                        onBlur={onBlurCpfValue}
                      />
                      {cpf && (
                        <InputRightElement
                          children={<CheckIcon color="green.500" />}
                        />
                      )}
                    </InputGroup>
                    {errors?.cpf?.message && (
                      <Text
                        fontFamily="Lato"
                        fontStyle="normal"
                        fontWeight="500"
                        fontSize="11px"
                        lineHeight="18px"
                        color="fail.500"
                      >
                        {errors.cpf.message}
                      </Text>
                    )}
                  </FormControl>
                </GridItem>
                <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
                  <FormControl w="100%">
                    <Fonts.FormLabelText>
                      Data de Nascimento
                    </Fonts.FormLabelText>
                    <Input
                      type="date"
                      textIndent={"1px"}
                      w="100%"
                      h={10}
                      borderRadius={3}
                      border="1px solid #EAEAEA"
                      bgColor="neutral.0"
                      {...register("birth")}
                    />
                  </FormControl>
                </GridItem>
                <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
                  <FormControl w="100%">
                    <Fonts.FormLabelText>Telefone</Fonts.FormLabelText>
                    <Input
                      {...register("contact")}
                      as={InputMask}
                      mask="(**) *****-****"
                      type="text"
                      textIndent={"1px"}
                      w="100%"
                      h={10}
                      borderRadius={3}
                      border="1px solid #EAEAEA"
                      bgColor="neutral.0"
                      onChange={onChangeContactValue}
                    />
                    {errors?.contact?.message && (
                      <Text
                        fontFamily="Lato"
                        fontStyle="normal"
                        fontWeight="500"
                        fontSize="11px"
                        lineHeight="18px"
                        color="fail.500"
                      >
                        {errors.contact.message}
                      </Text>
                    )}
                  </FormControl>
                </GridItem>
              </Grid>
              <Grid templateColumns="repeat(4, 1fr)" mt={8} mb={3} gap={5}>
                <GridItem colSpan={{ base: 4, md: 4, lg: 1 }}>
                  <Button
                    variant="outline"
                    borderColor="secondary.500"
                    colorScheme="secondary.500"
                    w="100%"
                    h={9}
                    borderRadius={4}
                    onClick={() => navigate("/")}
                    _hover={{
                      bgColor: "secondary.100",
                    }}
                    _active={{
                      bgColor: "secondary.300",
                    }}
                  >
                    <Fonts.ButtonText color="secondary.500">
                      Cancelar
                    </Fonts.ButtonText>
                  </Button>
                </GridItem>
                <GridItem colSpan={{ base: 4, md: 4, lg: 1 }}>
                  <Button
                    variant="outline"
                    borderColor="secondary.500"
                    colorScheme="secondary.500"
                    w="100%"
                    h={9}
                    borderRadius={4}
                    onClick={() => {
                      onOpen();
                      setModalType("password");
                    }}
                    _hover={{
                      bgColor: "secondary.100",
                    }}
                    _active={{
                      bgColor: "secondary.300",
                    }}
                  >
                    <Fonts.ButtonText color="secondary.500">
                      Alterar senha
                    </Fonts.ButtonText>
                  </Button>
                </GridItem>
                <GridItem colSpan={{ base: 4, md: 4, lg: 1 }}>
                  <Button
                    variant="outline"
                    borderColor="secondary.500"
                    colorScheme="secondary.500"
                    w="100%"
                    h={9}
                    borderRadius={4}
                    onClick={() => {
                      onOpen();
                      setModalType("e-mail");
                    }}
                    _hover={{
                      bgColor: "secondary.100",
                    }}
                    _active={{
                      bgColor: "secondary.300",
                    }}
                  >
                    <Fonts.ButtonText color="secondary.500">
                      Alterar e-mail
                    </Fonts.ButtonText>
                  </Button>
                </GridItem>
                <GridItem colSpan={{ base: 4, md: 4, lg: 1 }}>
                  <Button
                    variant="solid"
                    type="submit"
                    bgColor="secondary.500"
                    w="100%"
                    h={9}
                    borderRadius={4}
                    _hover={{
                      bgColor: "secondary.300",
                    }}
                    _active={{
                      bgColor: "secondary.500",
                    }}
                  >
                    <Fonts.ButtonText>Salvar Alterações</Fonts.ButtonText>
                  </Button>
                </GridItem>
              </Grid>
            </Box>
            {modalType === "password" && (
              <PasswordFormModal
                id={123}
                isOpen={isOpen}
                onClose={onModalFormClose}
                setIsLoading={setIsLoading}
              />
            )}
            {modalType === "e-mail" && (
              <EmailFormModal
                id={123}
                isOpen={isOpen}
                onClose={onModalFormClose}
                setIsLoading={setIsLoading}
                profileLoad={profileLoad}
              />
            )}
          </Stack>
        </Container>
      </Stack>
      <Footer />
    </Stack>
  );
};
