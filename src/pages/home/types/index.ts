import { FeePolicyEnum } from "@/pages/checkout/enums";

export type Product = {
  id: number;
  name: string;
  unitaryValue: string;
};

export type StoreEventDto = {
  id: number;
  name: string;
  slug: string;
  raffleDate?: string;
  initialDate: string;
  finalDate: string;
  raffleUrl?: string;
  feePolicy: FeePolicyEnum;
  videoUrl?: string;
  regulationUrl?: string;
  description?: string;
  products: Product[];
  desktopMediaUrl?: string;
  mobileMediaUrl?: string;
};

export type StoreEventFilterDto = {
  name: string;
};
