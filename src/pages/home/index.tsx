import { Container } from "@/components/container";
import { Footer } from "@/components/footer";
import { Header } from "@/components/header";
import { SearchIcon } from "@/components/icons/search";
import { ProductCard } from "@/components/productCard";
import SwiperSlideInfinite from "@/components/swiper";
import { useStoreEvent } from "@/hooks/useStore";
import {
  Grid,
  GridItem,
  HStack,
  Input,
  InputGroup,
  InputLeftElement,
  Stack,
} from "@chakra-ui/react";
import { useCallback, useEffect, useMemo, useState } from "react";
import { StoreEventDto, StoreEventFilterDto } from "./types";
import { useCustomToast } from "@/hooks/useToast";
import LoadingOverlay from "@/components/overlay";

const initialFilter: StoreEventFilterDto = {
  name: "",
};

/* eslint-disable react/no-children-prop */
export const Home = () => {
  const { getStoreEvents } = useStoreEvent();
  const { customToast } = useCustomToast();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [events, setEvents] = useState<StoreEventDto[]>([]);
  const [filter, setFilter] = useState<StoreEventFilterDto>(initialFilter);
  const [stopedTyping, setStopedTyping] = useState(false);
  const [typingTimer, setTypingTimer] = useState<NodeJS.Timeout | undefined>();

  const handleInputChange = () => {
    clearTimeout(typingTimer);

    const newTypingTimer = setTimeout(() => {
      setStopedTyping(true);
    }, 500);

    setTypingTimer(newTypingTimer);
  };

  const callbackStoreEvents = useCallback(() => {
    setIsLoading(true);

    getStoreEvents({ ...filter })
      .then((response) => setEvents(response.data))
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  }, [filter]);

  useEffect(() => {
    callbackStoreEvents();
  }, []);

  useEffect(() => {
    if (stopedTyping) {
      callbackStoreEvents();
      setStopedTyping(false);
    }
  }, [filter, stopedTyping]);

  const getEventsImage = useMemo(() => {
    return events.map((event) => {
      return {
        desktopMediaUrl: event.desktopMediaUrl!,
        mobileMediaUrl: event.mobileMediaUrl!,
      };
    });
  }, [events]);

  return (
    <Stack
      bg="neutral.200"
      h={isLoading || !events.length ? "100vh" : "100%"}
      w="100%"
      position="relative"
    >
      {isLoading && <LoadingOverlay />}
      <Header />
      <SwiperSlideInfinite images={getEventsImage} />
      <HStack
        bgColor="secondary.500"
        width="100%"
        h={12}
        justifyContent="center"
      >
        <InputGroup
          w={{ base: "70%", md: "70%", lg: "40%" }}
          alignItems="center"
        >
          <InputLeftElement
            justifyContent="center"
            pointerEvents="none"
            children={<SearchIcon />}
            top=""
          />
          <Input
            type="text"
            placeholder="Buscar por nome"
            _placeholder={{ color: "neutral.50" }}
            w="100%"
            h={9}
            borderRadius={4}
            textIndent="10px"
            border="1px solid #EAEAEA"
            bgColor="neutral.0"
            onChange={(event) => {
              setFilter({ ...filter, name: event.target.value });
              handleInputChange();
            }}
          />
        </InputGroup>
      </HStack>
      <Container>
        <Grid
          templateColumns={{
            base: "repeat(1, 1fr)",
            md: "repeat(1, 1fr)",
            lg: "repeat(2, 1fr)",
          }}
          gap={9}
          pt="10px"
          w="100%"
          mb={20}
        >
          {events.map((event) => (
            <GridItem key={event.id}>
              <ProductCard {...event} />
            </GridItem>
          ))}
        </Grid>
      </Container>
      <Footer />
    </Stack>
  );
};
