import {
  Button,
  Flex,
  FormControl,
  HStack,
  Input,
  Stack,
  Text,
  VStack,
} from "@chakra-ui/react";
import { SubmitHandler, useForm } from "react-hook-form";
import { joiResolver } from "@hookform/resolvers/joi";
import Joi from "joi";
import { ChangeEvent, useState } from "react";
import { useCustomToast } from "@/hooks/useToast";
import { useLogin } from "@/hooks/useLogin";
import { validateCpf } from "@/utils/validatorUtil";
import { StoreAccountDto } from "../login/types";
import { dateStringToDateFormat } from "@/utils/dateUtil";
import InputMask from "react-input-mask";
import { AccountBox } from "../../components/accountBox";
import { Header } from "@/components/header";
import { Footer } from "@/components/footer";
import { useNavigate } from "react-router-dom";

const schema = Joi.object({
  name: Joi.string().required().messages({
    "string.empty": "Nome deve ser informado",
  }),
  email: Joi.string().required().messages({
    "string.empty": "E-mail deve ser informado",
  }),
  cpf: Joi.string()
    .required()
    .pattern(/[0-9]/)
    .messages({
      "string.empty": "CPF deve ser informado",
      "string.invalid": "CPF inválido",
    })
    .custom((value, helper) => {
      const valueReplaced = value
        .replaceAll(".", "")
        .replaceAll("-", "")
        .replaceAll("_", "");

      const isValidCpf = validateCpf(valueReplaced);

      if (!isValidCpf || valueReplaced.length !== 11) {
        return helper.error("string.invalid");
      }

      return value;
    }),
  birth: Joi.string().allow(null, ""),
  password: Joi.string().min(6).required().messages({
    "string.empty": "Senha deve ser informada",
    "string.min": "Senha deve ter no mínimo 6 caracteres",
  }),
  passwordConfirmation: Joi.string()
    .required()
    .equal(Joi.ref("password"))
    .messages({
      "string.empty": "Confirmação de senha deve ser informada",
      "any.only": "Confirmação de senha não confere com a senha",
    }),
});

const initialValues = {
  name: "",
  email: "",
  cpf: "",
  password: "",
  passwordConfirmation: "",
};

export const NewAccount = () => {
  const navigate = useNavigate();
  const { doStore } = useLogin();
  const { customToast } = useCustomToast();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const {
    register,
    handleSubmit,
    watch,
    setValue,
    clearErrors,
    formState: { errors },
  } = useForm<StoreAccountDto>({
    defaultValues: initialValues,
    resolver: joiResolver(schema),
  });

  const cpf = watch("cpf");
  const email = watch("email");

  const onSubmit: SubmitHandler<StoreAccountDto> = (props) => {
    const data: StoreAccountDto = {
      ...props,
      cpf: props.cpf.replaceAll(".", "").replace("-", ""),
      birth: dateStringToDateFormat(props.birth),
    };

    setIsLoading(true);

    doStore(data)
      .then(() => {
        customToast("success", "Cadastro realizado com sucesso");
        navigate("/login");
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  const handleCpfValue = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value
      .replaceAll(".", "")
      .replaceAll("-", "")
      .replaceAll("_", "");

    const isValidNumberValue = /^[0-9]*$/.test(value);
    const validatedValue = isValidNumberValue ? value : cpf;

    setValue("cpf", validatedValue);
  };

  return (
    <Stack bg="neutral.100" h="100vh" w="100%">
      <Header />
      <Flex
        h="100%"
        w="100%"
        justifyContent="center"
        alignItems="center"
        p={{ base: "20px 20px", md: "20px 0" }}
      >
        <AccountBox>
          <VStack
            w="100%"
            spacing={7}
            as="form"
            onSubmit={handleSubmit(onSubmit)}
          >
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize="30px"
              lineHeight="36px"
              color="neutral.400"
            >
              Criar Conta
            </Text>
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="15px"
              lineHeight="18px"
              color="neutral.50"
            >
              Insira seus dados
            </Text>

            <VStack w="100%" spacing={7}>
              <FormControl w="100%">
                <Input
                  placeholder="Nome completo"
                  bg="neutral.0"
                  borderRadius="50px"
                  h={50}
                  size="md"
                  type="text"
                  _placeholder={{ color: "neutral.50" }}
                  w="100%"
                  textIndent="15px"
                  border={`1px solid ${
                    errors?.name?.message ? "red" : "#EAEAEA"
                  }`}
                  bgColor="neutral.0"
                  {...register("name")}
                />
                {errors?.name?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.name.message}
                  </Text>
                )}
              </FormControl>
              <FormControl w="100%">
                <Input
                  placeholder="E-mail"
                  bg="neutral.0"
                  borderRadius="50px"
                  h={50}
                  size="md"
                  type="email"
                  _placeholder={{ color: "neutral.50" }}
                  w="100%"
                  textIndent="15px"
                  border={`1px solid ${
                    errors?.email?.message ? "red" : "#EAEAEA"
                  }`}
                  bgColor="neutral.0"
                  value={email}
                  onChange={(event) => {
                    clearErrors("email");
                    setValue("email", event.target.value.toLowerCase());
                  }}
                />
                {errors?.email?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.email.message}
                  </Text>
                )}
              </FormControl>
            </VStack>
            <HStack w="100%" spacing={3}>
              <FormControl w="100%">
                <Input
                  {...register("cpf")}
                  as={InputMask}
                  mask="***.***.***-**"
                  placeholder="CPF"
                  bg="neutral.0"
                  borderRadius="50px"
                  h={50}
                  size="md"
                  type="text"
                  _placeholder={{ color: "neutral.50" }}
                  w="100%"
                  textIndent="15px"
                  border={`1px solid ${
                    errors?.cpf?.message ? "red" : "#EAEAEA"
                  }`}
                  bgColor="neutral.0"
                  onChange={handleCpfValue}
                />
                {errors?.cpf?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.cpf.message}
                  </Text>
                )}
              </FormControl>
              <FormControl w="100%">
                <Input
                  {...register("birth")}
                  as={InputMask}
                  mask="**/**/****"
                  placeholder="Data de nascimento"
                  bg="neutral.0"
                  borderRadius="50px"
                  h={50}
                  size="md"
                  type="text"
                  _placeholder={{ color: "neutral.50" }}
                  w="100%"
                  textIndent="15px"
                  border="1px solid #EAEAEA"
                  bgColor="neutral.0"
                />
              </FormControl>
            </HStack>
            <VStack w="100%" spacing={7}>
              <FormControl w="100%">
                <Input
                  placeholder="Senha"
                  bg="neutral.0"
                  borderRadius="50px"
                  h={50}
                  size="md"
                  type="password"
                  _placeholder={{ color: "neutral.50" }}
                  w="100%"
                  textIndent="15px"
                  border={`1px solid ${
                    errors?.password?.message ? "red" : "#EAEAEA"
                  }`}
                  bgColor="neutral.0"
                  {...register("password")}
                />
                {errors?.password?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.password.message}
                  </Text>
                )}
              </FormControl>
              <FormControl w="100%">
                <Input
                  placeholder="Confirmar senha"
                  bg="neutral.0"
                  borderRadius="50px"
                  h={50}
                  size="md"
                  type="password"
                  _placeholder={{ color: "neutral.50" }}
                  w="100%"
                  textIndent="15px"
                  border={`1px solid ${
                    errors?.passwordConfirmation?.message ? "red" : "#EAEAEA"
                  }`}
                  bgColor="neutral.0"
                  {...register("passwordConfirmation")}
                />
                {errors?.passwordConfirmation?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.passwordConfirmation.message}
                  </Text>
                )}
              </FormControl>
            </VStack>
            <Button
              w="100%"
              borderRadius="50px"
              backgroundColor="neutral.700"
              h="50px"
              colorScheme="neutral.700"
              variant="solid"
              type="submit"
              isLoading={isLoading}
              _hover={{
                bgColor: "neutral.400",
              }}
              _active={{
                bgColor: "neutral.700",
              }}
            >
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="16px"
                lineHeight="24px"
                cursor="pointer"
              >
                cadastrar
              </Text>
            </Button>

            <HStack w="100%" justifyContent="center">
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="15px"
                lineHeight="18px"
                color="secondary.600"
              >
                Li e concordo com a{" "}
              </Text>
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="15px"
                lineHeight="18px"
                color="secondary.500"
                cursor="pointer"
              >
                política de privacidade
              </Text>
            </HStack>
          </VStack>
        </AccountBox>
      </Flex>
      <Footer />
    </Stack>
  );
};
