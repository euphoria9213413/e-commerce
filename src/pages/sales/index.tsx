import {
  Input,
  InputGroup,
  InputLeftElement,
  Stack,
  useBreakpointValue,
} from "@chakra-ui/react";
import { MobileContent } from "./mobileContent";
import { DesktopContent } from "./desktopContent";
import { Header } from "@/components/header";
import { FilterBar } from "@/components/FilterBar";
import { SearchIcon } from "@/components/icons/search";
import { SaleIndexDto, SaleIndexFilterDto } from "./types";
import { useCallback, useEffect, useState } from "react";
import { Footer } from "@/components/footer";
import { useCustomToast } from "@/hooks/useToast";
import { useSale } from "@/hooks/useSale";
import LoadingOverlay from "@/components/overlay";

const initialFilter: SaleIndexFilterDto = {
  graduateCpf: "",
  storeName: "",
};

/* eslint-disable react/no-children-prop */
export const Sales = () => {
  const { getSales, resendEmail } = useSale();
  const [filter, setFilter] = useState<SaleIndexFilterDto>(initialFilter);
  const [typingTimer, setTypingTimer] = useState<NodeJS.Timeout | undefined>();
  const [stopedTyping, setStopedTyping] = useState(false);
  const { customToast } = useCustomToast();
  const [page, setPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [sales, setSales] = useState<SaleIndexDto[]>([]);
  const [totalSales, setTotalSales] = useState<number>(0);
  const [lastPage, setLastPage] = useState<number>(1);

  const isTablet = useBreakpointValue({ base: true, md: true, lg: false });

  const handleInputChange = () => {
    clearTimeout(typingTimer);

    const newTypingTimer = setTimeout(() => {
      setStopedTyping(true);
    }, 500);

    setTypingTimer(newTypingTimer);
  };

  const callbackSales = useCallback(() => {
    setIsLoading(true);

    getSales({ page, ...filter })
      .then((response) => {
        setSales(response.data.data);
        setTotalSales(response.data.total);
        setLastPage(response.data.lastPage);
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  }, [page, filter]);

  useEffect(() => {
    callbackSales();
  }, [page]);

  useEffect(() => {
    if (stopedTyping) {
      if (page === 1) {
        callbackSales();
      } else {
        setPage(1);
      }
      setStopedTyping(false);
    }
  }, [page, filter, stopedTyping]);

  const onResentEmail = (saleId: number) => {
    setIsLoading(true);

    resendEmail(saleId)
      .then(() => customToast("success", "E-mail reenviado com sucesso"))
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  const filters = [
    {
      field: (
        <InputGroup alignItems="center" key={1}>
          <InputLeftElement
            pointerEvents="none"
            children={<SearchIcon />}
            top=""
            left={1}
          />
          <Input
            type="text"
            placeholder="Buscar pelo CPF do formando"
            _placeholder={{ color: "neutral.50" }}
            w="100%"
            h={9}
            borderRadius={4}
            textIndent="10px"
            border="1px solid #EAEAEA"
            bgColor="neutral.0"
            onChange={(event) => {
              setFilter({
                ...filter,
                graduateCpf: event.target.value
                  .replaceAll(".", "")
                  .replaceAll("-", ""),
              });
              handleInputChange();
              setPage(1);
            }}
          />
        </InputGroup>
      ),
    },
    {
      field: (
        <InputGroup alignItems="center" key={2}>
          <InputLeftElement
            pointerEvents="none"
            children={<SearchIcon />}
            top=""
            left={1}
          />
          <Input
            type="text"
            placeholder="Buscar pelo nome da loja"
            _placeholder={{ color: "neutral.50" }}
            w="100%"
            h={9}
            borderRadius={4}
            textIndent="10px"
            border="1px solid #EAEAEA"
            bgColor="neutral.0"
            isDisabled={!sales.length && !filter.storeName}
            onChange={(event) => {
              setFilter({
                ...filter,
                storeName: event.target.value,
              });
              handleInputChange();
              setPage(1);
            }}
          />
        </InputGroup>
      ),
    },
  ];

  return (
    <Stack
      h={sales.length < 10 ? "100vh" : "100%"}
      w="100%"
      position="relative"
      gap={0}
    >
      <Header />
      <FilterBar
        title={`Vendas em meu nome (${totalSales})`}
        subTitle="Lista de compras realizadas por formando"
        filters={filters}
      />
      {isLoading && <LoadingOverlay />}
      {isTablet ? (
        <MobileContent
          sales={sales}
          page={page}
          setPage={setPage}
          lastPage={lastPage}
          onResentEmail={onResentEmail}
        />
      ) : (
        <DesktopContent
          sales={sales}
          page={page}
          setPage={setPage}
          lastPage={lastPage}
          onResentEmail={onResentEmail}
        />
      )}
      {!isTablet && <Footer />}
    </Stack>
  );
};
