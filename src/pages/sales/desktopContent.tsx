import {
  Stack,
  Table,
  TableContainer,
  Tbody,
  Td,
  Tfoot,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import * as Font from "@/components/texts";
import { MenuActionButton } from "@/components/menuActionButton";
import { useState } from "react";
import { MenuPopUp } from "@/components/menuPopUp";
import { Pagination } from "@/components/pagination";
import * as T from "@/components/table";
import { Props, SaleIndexDto } from "./types";
import { moneyFormat } from "@/utils/formatUtil";

type DesktopProps = {
  sales: SaleIndexDto[];
} & Props;

export const DesktopContent = ({
  sales,
  page,
  lastPage,
  setPage,
  onResentEmail,
}: DesktopProps) => {
  const [showMenuPopUp, setShowMenuPopUp] = useState<undefined | number>();

  return (
    <Stack h="100%" w="100%" p={5}>
      <TableContainer>
        <Table w="100%">
          <Thead>
            <Tr>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Data e hora</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Código do formando</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Nome formando</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Nome comprador</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Número da sorte</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Nome loja</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Produto</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Valor arrecadado</Font.TableTheadText>
              </Th>
              <Th></Th>
            </Tr>
          </Thead>
          <Tbody>
            {sales.map((sale) => (
              <T.Body key={sale.id}>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{`${sale.date} ${sale.hour}`}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{sale.graduateCode}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{sale.graduateName}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{sale.buyerName}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{sale.luckyNumber}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{sale.storeName}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{sale.productName}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>
                    {moneyFormat(Number(sale.graduateMargin))}
                  </Font.TableBodyText>
                </Td>
                <Td>
                  <MenuActionButton
                    color="#2F2F2F"
                    onClick={() => setShowMenuPopUp(sale.id)}
                    _hover={{
                      bgColor: "neutral.200",
                    }}
                    _active={{
                      bgColor: "neutral.300",
                    }}
                  />
                </Td>
                {showMenuPopUp === sale.id && (
                  <MenuPopUp
                    top="-4px"
                    right="36px"
                    options={[
                      {
                        title: "Reenviar e-mail",
                        onClick: () => onResentEmail(sale.id),
                      },
                    ]}
                    setShowMenuPopUp={setShowMenuPopUp}
                  />
                )}
              </T.Body>
            ))}
          </Tbody>
          <Tfoot w="100%" h="15px"></Tfoot>
        </Table>
        <Pagination page={page} lastPage={lastPage} onClick={setPage} />
      </TableContainer>
    </Stack>
  );
};
