import { PaymentStatusType } from "../../orders/enums";

export type OrderIndexFilterDto = {
  buyerNameOrBuyerEmailOrGraduateName: string;
};

export type Props = {
  page: number;
  lastPage: number;
  setPage: (value: number) => void;
  onResentEmail: (id: number) => void;
};

export type SaleIndexFilterDto = {
  graduateCpf: string;
  storeName: string;
};

export type SaleIndexDto = {
  id: number;
  luckyNumber: string;
  graduateCode: string;
  graduateName: string;
  buyerName: string;
  buyerContact: string;
  buyerEmail: string;
  storeName: string;
  price: string;
  graduateMargin: string;
  date: string;
  hour: string;
  productName: string;
  paymentStatus: PaymentStatusType;
  paymentType: string;
  paymentCopyPaste?: string;
  expirationDate?: string;
  expirationHour?: string;
};

export type SaleIndexPaginatedDto = {
  data: SaleIndexDto[];
  page: number;
  perPage: number;
  lastPage: number;
  total: number;
};

export type SaleIndexQueryDto = {
  graduateCpf?: string;
  storeName?: string;
  page: number;
};

export type OrderIndexQueryDto = {
  buyerNameOrBuyerEmailOrGraduateName?: string;
  page: number;
};
