import { Box, HStack, Stack, Text, VStack } from "@chakra-ui/react";
import { Props, SaleIndexDto } from "./types";
import { useEffect, useState } from "react";
import { Footer } from "@/components/footer";
import InfiniteScroll from "react-infinite-scroll-component";
import { moneyFormat } from "@/utils/formatUtil";

type MobileProps = {
  sales: SaleIndexDto[];
} & Props;

export const MobileContent = ({
  sales,
  page,
  lastPage,
  setPage,
  onResentEmail,
}: MobileProps) => {
  const [mobileSales, setMobileSales] = useState<SaleIndexDto[]>(sales);

  useEffect(() => {
    if (page > 1) return setMobileSales([...mobileSales, ...sales]);

    setMobileSales(sales);
  }, [sales]);

  const heiht = mobileSales.length > 1 ? {} : { h: "100%" };

  return (
    <VStack
      bgColor="neutral.200"
      {...heiht}
      w="100%"
      flexDirection="column-reverse"
    >
      <Footer />
      <VStack h="100%" w="100%" p="0 10px" my={4}>
        <InfiniteScroll
          style={{ backgroundColor: "#EAEAEA" }}
          dataLength={mobileSales.length}
          next={() => {
            setPage(page + 1);
          }}
          hasMore={page < lastPage}
          loader={<></>}
        >
          <Stack spacing={3} w="100%">
            {mobileSales.map((sale) => (
              <Box
                key={sale.id}
                w={{ base: "360px", md: "500px" }}
                bgColor="neutral.0"
                pt={3}
                borderRadius={4}
              >
                <Stack spacing={5}>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Data e hora
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {`${sale.date} ${sale.hour}`}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Código do formando
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {sale.graduateCode}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Nome formando
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {sale.graduateName}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Nome comprador
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {sale.buyerName}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Número da sorte
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {sale.luckyNumber}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Nome loja
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {sale.storeName}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Produto
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {sale.productName}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Valor arrecadado
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {moneyFormat(Number(sale.graduateMargin))}
                    </Text>
                  </VStack>
                </Stack>
                <HStack justifyContent="flex-end" mr={3} mt={3}>
                  <Text
                    fontFamily="Inter"
                    fontStyle="normal"
                    fontWeight="700"
                    fontSize={{ base: "12px", md: "14px" }}
                    lineHeight="18px"
                    color="blue.800"
                    textDecoration="underline"
                    onClick={() => onResentEmail(sale.id)}
                  >
                    reenviar e-mail
                  </Text>
                </HStack>
              </Box>
            ))}
          </Stack>
        </InfiniteScroll>
      </VStack>
    </VStack>
  );
};
