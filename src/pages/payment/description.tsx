import { HStack, Stack, Text } from "@chakra-ui/react";
import { moneyFormat } from "@/utils/formatUtil";

type Props = {
  orderTotal: number;
};

export const Description = ({ orderTotal }: Props) => {
  return (
    <Stack spacing={10}>
      <Text
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="700"
        fontSize={{ base: "25px", md: "30px" }}
        lineHeight="48px"
        color="secondary.500"
      >
        Forma de Pagamento
      </Text>
      <Stack spacing={3}>
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="600"
          fontSize={{ base: "18px", md: "20px" }}
          lineHeight="20px"
          color="neutral.600"
        >
          Pix
        </Text>
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="400"
          fontSize={{ base: "18px", md: "20px" }}
          lineHeight="20px"
          color="neutral.600"
        >
          {`Total do pedido: ${moneyFormat(Number(orderTotal))}`}
        </Text>
      </Stack>
      <Stack spacing={3}>
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="600"
          fontSize={{ base: "18px", md: "20px" }}
          lineHeight="20px"
          color="neutral.600"
        >
          Agora é só pagar o Pix 😉
        </Text>
        <Stack ml={10} spacing={{ base: 1, md: 1, lg: 4 }}>
          <HStack>
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="900"
              fontSize={{ base: "25px", md: "30px" }}
              lineHeight="48px"
              color="secondary.500"
              mr={10}
            >
              1
            </Text>
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize={{ base: "18px", md: "20px" }}
              lineHeight="20px"
              color="neutral.600"
            >
              Acesse seu Internet Banking ou app de pagamentos.
            </Text>
          </HStack>
          <HStack>
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="900"
              fontSize={{ base: "25px", md: "30px" }}
              lineHeight="48px"
              color="secondary.500"
              mr={10}
            >
              2
            </Text>
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize={{ base: "18px", md: "20px" }}
              lineHeight="20px"
              color="neutral.600"
            >
              Escolha pagar via Pix.
            </Text>
          </HStack>
          <HStack>
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="900"
              fontSize={{ base: "25px", md: "30px" }}
              lineHeight="48px"
              color="secondary.500"
              mr={10}
            >
              3
            </Text>
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize={{ base: "18px", md: "20px" }}
              lineHeight="20px"
              color="neutral.600"
            >
              Copia e cola o código do pagamento ou escaneie o QR Code.
            </Text>
          </HStack>
        </Stack>
      </Stack>
    </Stack>
  );
};
