import {
  Button,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  VStack,
} from "@chakra-ui/react";
import * as Fonts from "@/components/texts";
import { useNavigate } from "react-router-dom";

type Props = {
  isOpen: boolean;
  onClose: () => void;
};

export const OrderModal = ({ isOpen, onClose }: Props) => {
  const navigate = useNavigate();

  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <ModalOverlay />
      <ModalContent bgColor="neutral.100" w="300px">
        <ModalHeader>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="700"
            fontSize="24px"
            lineHeight="32px"
            color="neutral.600"
            textAlign="center"
          >
            Pedido realizado!
          </Text>
        </ModalHeader>
        <ModalBody>
          <VStack spacing={2}>
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="700"
              fontSize="14px"
              lineHeight="22px"
              color="neutral.600"
              textAlign="center"
            >
              Sua compra foi concluída!
            </Text>
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="500"
              fontSize="14px"
              lineHeight="22px"
              color="neutral.600"
              textAlign="center"
            >
              Após identificarmos o pagamento, você receberá um e-mail com seus
              <b> números da sorte.</b>
            </Text>
          </VStack>
        </ModalBody>
        <ModalFooter>
          <VStack w="100%" spacing={5}>
            <Button
              variant="outline"
              borderColor="secondary.500"
              colorScheme="secondary.500"
              w="100%"
              h={9}
              borderRadius={4}
              onClick={() => {
                onClose;
                navigate("/");
              }}
              _hover={{
                bgColor: "secondary.100",
              }}
              _active={{
                bgColor: "secondary.300",
              }}
            >
              <Fonts.ButtonText color="secondary.500">
                Página Inicial
              </Fonts.ButtonText>
            </Button>
            <Button
              variant="solid"
              bgColor="secondary.500"
              w="100%"
              h={9}
              borderRadius={4}
              onClick={() => {
                navigate("/pedidos");
              }}
              _hover={{
                bgColor: "secondary.300",
              }}
              _active={{
                bgColor: "secondary.500",
              }}
            >
              <Fonts.ButtonText>Meus Pedidos</Fonts.ButtonText>
            </Button>
          </VStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
