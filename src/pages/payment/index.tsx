import { Container } from "@/components/container";
import { Footer } from "@/components/footer";
import { Header } from "@/components/header";
import {
  Box,
  Grid,
  GridItem,
  Image,
  Stack,
  useBreakpointValue,
  useTimeout,
} from "@chakra-ui/react";
import { Description } from "./description";
import { QrCodeBox } from "./qrCodeBox";
import { useEffect, useState } from "react";
import { useCheckout } from "@/hooks/useCheckout";
import { CheckoutDataDto } from "../checkout/types";
import { useNavigate } from "react-router-dom";

export const Payment = () => {
  const { getCheckout, removeCheckout } = useCheckout();
  const [orderTotal, setOrderTotal] = useState<number | undefined>(0);
  const navigate = useNavigate();
  const checkout = getCheckout();
  const checkoutItems: CheckoutDataDto = checkout ? JSON.parse(checkout) : "";
  const isMobile = useBreakpointValue({ base: true, md: false });

  useEffect(() => {
    if (!checkout) return navigate("/");
    if (orderTotal === undefined) return navigate("/");

    setOrderTotal(
      checkoutItems?.orderTotal ? checkoutItems.orderTotal : undefined
    );
  }, []);

  useTimeout(() => removeCheckout(), 5000);

  return (
    <Stack minH="100vh" w="100%" position="relative">
      <Header />
      <Image
        src={
          isMobile
            ? checkoutItems.mobileBannerImageUrl
            : checkoutItems.desktopBannerImageUrl
        }
        maxH={{ base: "250px", md: "472px" }}
        mt={1}
      ></Image>
      <Box bgColor="secondary.500" width="100%" h={6} mt={2}></Box>
      <Container>
        <Box
          p="15px 45px"
          bgColor="neutral.0"
          borderRadius={6}
          border="solid 1px"
          borderColor="neutral.10"
        >
          <Grid
            w="100%"
            m="10px 0"
            templateColumns={{
              base: "repeat(1, 1fr)",
              md: "repeat(3, 1fr)",
              lg: "repeat(3, 1fr)",
            }}
            gap={8}
          >
            <GridItem colSpan={{ base: 3, md: 3, lg: 2 }}>
              <Description orderTotal={orderTotal!} />
            </GridItem>
            <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
              <QrCodeBox
                pixCopyPasteValue={checkoutItems.pixCopyPasteValue}
                expirationDate={checkoutItems.expirationDate}
                expirationHour={checkoutItems.expirationHour}
                openOrderPageRedirectModal={true}
              />
            </GridItem>
          </Grid>
        </Box>
      </Container>
      <Footer />
    </Stack>
  );
};
