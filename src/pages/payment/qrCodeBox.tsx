import { useCustomToast } from "@/hooks/useToast";
import {
  Button,
  Stack,
  Text,
  useBreakpointValue,
  useDisclosure,
} from "@chakra-ui/react";
import { OrderModal } from "./orderModal";
import { QRCodeCanvas } from "qrcode.react";

type Props = {
  pixCopyPasteValue?: string;
  expirationDate?: string;
  expirationHour?: string;
  openOrderPageRedirectModal?: boolean;
};

export const QrCodeBox = ({
  pixCopyPasteValue,
  expirationDate,
  expirationHour,
  openOrderPageRedirectModal,
}: Props) => {
  const { customToast } = useCustomToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const isMobile = useBreakpointValue({ base: true, md: false });

  return (
    <Stack justifyContent="center" alignItems="center">
      <Stack
        bgColor="neutral.150"
        h={{ base: "180px", md: "350px" }}
        w={{ base: "180px", md: "350px" }}
        justifyContent="center"
        alignItems="center"
        borderRadius={6}
        mb={10}
      >
        {pixCopyPasteValue && (
          <QRCodeCanvas value={pixCopyPasteValue} size={isMobile ? 150 : 300} />
        )}
      </Stack>
      <Button
        w={{ base: "180px", md: "350px" }}
        variant="ghost"
        bgColor="secondary.500"
        onClick={() => {
          navigator.clipboard.writeText(pixCopyPasteValue ?? "");
          customToast("success", "QRCode copiado com sucesso");
          openOrderPageRedirectModal && onOpen();
        }}
        _hover={{
          bgColor: "secondary.300",
        }}
        _active={{
          bgColor: "secondary.500",
        }}
        mb={3}
      >
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="600"
          fontSize="16px"
          lineHeight="20px"
          color="neutral.0"
        >
          Copiar código Pix
        </Text>
      </Button>
      <Text
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="400"
        fontSize="16px"
        lineHeight="16px"
        color="secondary.500"
        w={{ base: "180px", md: "350px" }}
      >
        Realizar o pagamento até {expirationDate} às {expirationHour} horário de
        Brasília.
      </Text>
      <OrderModal isOpen={isOpen} onClose={onClose} />
    </Stack>
  );
};
