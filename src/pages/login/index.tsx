import {
  Button,
  Flex,
  FormControl,
  HStack,
  Input,
  Stack,
  Text,
  VStack,
} from "@chakra-ui/react";
import { SubmitHandler, useForm } from "react-hook-form";
import { joiResolver } from "@hookform/resolvers/joi";
import Joi from "joi";
import { LoginSubmitDto } from "./types";
import { useLocation, useNavigate } from "react-router-dom";
import { useState } from "react";
import { useCustomToast } from "@/hooks/useToast";
import { useLogin } from "@/hooks/useLogin";
import { AccountBox } from "../../components/accountBox";
import { Header } from "@/components/header";
import { Footer } from "@/components/footer";

const schema = Joi.object({
  email: Joi.string().required().messages({
    "string.empty": "E-mail deve ser informado",
  }),
  password: Joi.string().required().messages({
    "string.empty": "Senha deve ser informada",
  }),
});

export const Login = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const previousPath = location.state?.from ?? "";
  const { doLogin, setAuthentication } = useLogin();
  const { customToast } = useCustomToast();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginSubmitDto>({
    resolver: joiResolver(schema),
  });

  const onSubmit: SubmitHandler<LoginSubmitDto> = (props) => {
    setIsLoading(true);

    doLogin(props)
      .then((response) => {
        setAuthentication(response.data);

        if (previousPath.includes("evento")) {
          return navigate(-1);
        }

        navigate("/");
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  return (
    <Stack bg="neutral.100" h="100vh" w="100%">
      <Header />
      <Flex
        h="100%"
        w="100%"
        justifyContent="center"
        alignItems="center"
        p={{ base: "0 20px", md: "0 0" }}
      >
        <AccountBox>
          <VStack
            w="100%"
            spacing={7}
            as="form"
            onSubmit={handleSubmit(onSubmit)}
          >
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize="30px"
              lineHeight="36px"
              color="neutral.400"
            >
              Entrar
            </Text>
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="15px"
              lineHeight="18px"
              color="neutral.50"
            >
              Insira seus dados
            </Text>

            <VStack w="100%" spacing={7}>
              <FormControl w="100%">
                <Input
                  type="email"
                  placeholder="E-mail"
                  _placeholder={{ color: "neutral.50" }}
                  w="100%"
                  h={50}
                  borderRadius="50px"
                  textIndent="15px"
                  border={`1px solid ${
                    errors?.email?.message ? "red" : "#EAEAEA"
                  }`}
                  bgColor="neutral.0"
                  {...register("email")}
                />
                {errors?.email?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.email.message}
                  </Text>
                )}
              </FormControl>
              <FormControl w="100%">
                <Input
                  type="password"
                  placeholder="Senha"
                  _placeholder={{ color: "neutral.50" }}
                  w="100%"
                  h={50}
                  borderRadius="50px"
                  textIndent="15px"
                  border={`1px solid ${
                    errors?.password?.message ? "red" : "#EAEAEA"
                  }`}
                  bgColor="neutral.0"
                  {...register("password")}
                />
                {errors?.password?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.password.message}
                  </Text>
                )}
              </FormControl>
            </VStack>
            <HStack w="100%" justifyContent="flex-end">
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="15px"
                lineHeight="18px"
                color="secondary.500"
                cursor="pointer"
                onClick={() => navigate("/recuperar-senha")}
              >
                Recuperar senha
              </Text>
            </HStack>
            <Button
              w="100%"
              borderRadius="50px"
              backgroundColor="neutral.700"
              textColor="neutral.0"
              h="50px"
              colorScheme="neutral.700"
              variant="solid"
              type="submit"
              isLoading={isLoading}
              _hover={{
                bgColor: "neutral.400",
              }}
              _active={{
                bgColor: "neutral.700",
              }}
            >
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="16px"
                lineHeight="24px"
                cursor="pointer"
              >
                entrar
              </Text>
            </Button>
            <HStack w="100%" justifyContent="center">
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="15px"
                lineHeight="18px"
                color="neutral.400"
              >
                Não tem uma conta?
              </Text>
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="15px"
                lineHeight="18px"
                color="secondary.500"
                cursor="pointer"
                onClick={() => navigate("/criar-conta")}
              >
                Cadastre-se
              </Text>
            </HStack>
          </VStack>
        </AccountBox>
      </Flex>
      <Footer />
    </Stack>
  );
};
