import {
  Box,
  Checkbox,
  FormControl,
  Grid,
  GridItem,
  HStack,
  Input,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import { GraduateSearchModal } from "./graduateSearchModal";
import {
  CheckoutFormDataDto,
  CheckoutFormDto,
  GraduateIndexDto,
  GraduateIndexFilterDto,
} from "./types";
import { Product } from "../home/types";
import {
  FieldErrors,
  UseFormClearErrors,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import { useCallback, useMemo } from "react";
import InputMask from "react-input-mask";

type Props = {
  graduates: GraduateIndexDto[];
  product: Product;
  quantity: number;
  index: number;
  watch: UseFormWatch<CheckoutFormDto>;
  setValue: UseFormSetValue<CheckoutFormDto>;
  errors: FieldErrors<CheckoutFormDto>;
  handleInputChange: () => void;
  filter: GraduateIndexFilterDto;
  setFilter: (value: GraduateIndexFilterDto) => void;
  clearErrors: UseFormClearErrors<CheckoutFormDto>;
};

export const DataForm = ({
  graduates,
  product,
  quantity,
  index,
  watch,
  setValue,
  errors,
  handleInputChange,
  filter,
  setFilter,
  clearErrors,
}: Props) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const sales = watch("sales");
  const saleIndex = useMemo(
    () =>
      sales.findIndex(
        (sale) => sale.productId === product.id && sale.index === index
      ),
    [sales]
  );
  const currentSale = useMemo(
    () =>
      sales.find(
        (sale) => sale.productId === product.id && sale.index === index
      ),
    [sales]
  );

  const setSaleData = useCallback(
    (newSale: CheckoutFormDataDto) => {
      const newSales = [
        ...sales.slice(0, saleIndex),
        newSale,
        ...sales.slice(saleIndex! + 1),
      ];

      setValue("sales", newSales);
    },
    [product, index, sales]
  );

  const onReplicateForAll = (isChecked: boolean) => {
    const allOtherSales = sales.filter(
      (sale) =>
        sale.productId === currentSale?.productId &&
        sale.index !== currentSale.index
    );

    const replicatedSales = allOtherSales.reduce(
      (acc, curr) => {
        const otherSaleIndex = acc.findIndex(
          (sale) =>
            sale.productId === curr.productId && sale.index === curr.index
        );

        const newSale = {
          ...curr,
          graduateId: isChecked ? currentSale!.graduateId : undefined,
          graduateName: isChecked ? currentSale!.graduateName : undefined,
          buyerName: isChecked ? currentSale!.buyerName : undefined,
          buyerEmail: isChecked ? currentSale!.buyerEmail : undefined,
          buyerContact: isChecked ? currentSale!.buyerContact : undefined,
        };

        acc = [
          ...acc.slice(0, otherSaleIndex),
          newSale,
          ...acc.slice(otherSaleIndex! + 1),
        ];

        return acc;
      },
      [...sales]
    );

    setValue("sales", replicatedSales);
  };

  const contactWithoutMask = (contact: string) =>
    contact
      .replaceAll("(", "")
      .replaceAll(")", "")
      .replaceAll(" ", "")
      .replaceAll("-", "")
      .replaceAll("_", "");

  return (
    <>
      <Box
        bgColor="neutral.0"
        borderRadius={7}
        w="100%"
        border="solid 1px"
        borderColor="neutral.10"
        p={5}
        mb={5}
      >
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="700"
          fontSize={{ base: "16px", md: "18px" }}
          lineHeight="24px"
          color="secondary.300"
        >
          {quantity > 1
            ? `${product.name} - Formulário ${index}/${quantity}`
            : `${product.name} - Formulário`}
        </Text>
        <Grid
          templateColumns={{
            base: "repeat(1, 1fr)",
            md: "repeat(1, 1fr)",
            lg: "repeat(2, 1fr)",
          }}
          gap={4}
          w="100%"
        >
          <GridItem>
            <FormControl w="100%">
              <HStack justifyContent="space-between">
                <Text
                  fontFamily="Inter"
                  fontStyle="normal"
                  fontWeight="400"
                  fontSize={{ base: "10px", md: "12px" }}
                  lineHeight="18px"
                  color="neutral.500"
                >
                  Nome completo do formando*
                </Text>
                <Text
                  fontFamily="Inter"
                  fontStyle="normal"
                  fontWeight="700"
                  fontSize={{ base: "10px", md: "12px" }}
                  lineHeight="18px"
                  color="blue.800"
                  textDecoration="underline"
                  cursor="pointer"
                  onClick={onOpen}
                >
                  consulte aqui
                </Text>
              </HStack>
              <Input
                type="text"
                w="100%"
                h={10}
                borderRadius={4}
                border={`1px solid ${
                  errors?.sales &&
                  errors?.sales[saleIndex]?.graduateName?.message
                    ? "red"
                    : "#EAEAEA"
                }`}
                bgColor="neutral.0"
                isDisabled={true}
                value={currentSale?.graduateName || ""}
              />
              {errors?.sales &&
                errors?.sales[saleIndex]?.graduateName?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors?.sales[saleIndex]?.graduateName?.message}
                  </Text>
                )}
            </FormControl>
          </GridItem>
          <GridItem>
            <FormControl w="100%">
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="400"
                fontSize={{ base: "10px", md: "12px" }}
                lineHeight="18px"
                color="neutral.500"
              >
                Nome completo do comprador*
              </Text>
              <Input
                type="text"
                w="100%"
                h={10}
                borderRadius={4}
                border={`1px solid ${
                  errors?.sales && errors?.sales[saleIndex]?.buyerName?.message
                    ? "red"
                    : "#EAEAEA"
                }`}
                bgColor="neutral.0"
                value={currentSale?.buyerName || ""}
                onChange={(event) => {
                  clearErrors(`sales.${saleIndex}.buyerName`);

                  const value = event.target.value;
                  const newSale = {
                    ...currentSale!,
                    buyerName: value,
                  };

                  setSaleData(newSale);
                }}
              />
              {errors?.sales &&
                errors?.sales[saleIndex]?.buyerName?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors?.sales[saleIndex]?.buyerName?.message}
                  </Text>
                )}
            </FormControl>
          </GridItem>
          <GridItem>
            <FormControl w="100%">
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="400"
                fontSize={{ base: "10px", md: "12px" }}
                lineHeight="18px"
                color="neutral.500"
              >
                E-mail do comprador*
              </Text>
              <Input
                type="email"
                w="100%"
                h={10}
                borderRadius={4}
                border={`1px solid ${
                  errors?.sales && errors?.sales[saleIndex]?.buyerEmail?.message
                    ? "red"
                    : "#EAEAEA"
                }`}
                bgColor="neutral.0"
                value={currentSale?.buyerEmail || ""}
                onChange={(event) => {
                  clearErrors(`sales.${saleIndex}.buyerEmail`);

                  const value = event.target.value;
                  const newSale = {
                    ...currentSale!,
                    buyerEmail: value,
                  };

                  setSaleData(newSale);
                }}
              />
              {errors?.sales &&
                errors?.sales[saleIndex]?.buyerEmail?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors?.sales[saleIndex]?.buyerEmail?.message}
                  </Text>
                )}
            </FormControl>
          </GridItem>
          <GridItem>
            <FormControl w="100%">
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="400"
                fontSize={{ base: "10px", md: "12px" }}
                lineHeight="18px"
                color="neutral.500"
              >
                Telefone do comprador*
              </Text>
              <Input
                type="text"
                as={InputMask}
                mask="(**) *****-****"
                w="100%"
                h={10}
                borderRadius={4}
                border={`1px solid ${
                  errors?.sales &&
                  errors?.sales[saleIndex]?.buyerContact?.message
                    ? "red"
                    : "#EAEAEA"
                }`}
                bgColor="neutral.0"
                value={currentSale?.buyerContact || ""}
                onChange={(event) => {
                  clearErrors(`sales.${saleIndex}.buyerContact`);

                  const withoutMask = contactWithoutMask(event.target.value);

                  const isValidNumberValue = /^[0-9]*$/.test(withoutMask);
                  const validatedValue = isValidNumberValue
                    ? withoutMask
                    : currentSale?.buyerContact;

                  const newSale = {
                    ...currentSale!,
                    buyerContact: validatedValue,
                  };

                  setSaleData(newSale);
                }}
              />
              {errors?.sales &&
                errors?.sales[saleIndex]?.buyerContact?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors?.sales[saleIndex]?.buyerContact?.message}
                  </Text>
                )}
            </FormControl>
          </GridItem>
        </Grid>
        <Box m={10}>
          <GraduateSearchModal
            isOpen={isOpen}
            onClose={onClose}
            handleInputChange={handleInputChange}
            filter={filter}
            setFilter={setFilter}
            graduates={graduates}
            saleIndex={saleIndex}
            sale={currentSale!}
            clearErrors={clearErrors}
            setSaleData={setSaleData}
          />
        </Box>
      </Box>
      {quantity! > 1 && index === 1 && (
        <Checkbox
          size="lg"
          mb={5}
          borderColor="neutral.10"
          onChange={(event) => onReplicateForAll(event.target.checked)}
        >
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="600"
            fontSize={{ base: "12px", md: "14px" }}
            lineHeight="18px"
            color="neutral.500"
          >
            Replicar para todos
          </Text>
        </Checkbox>
      )}
    </>
  );
};
