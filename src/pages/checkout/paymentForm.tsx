import {
  Box,
  Button,
  FormControl,
  Grid,
  GridItem,
  HStack,
  Input,
  Radio,
  RadioGroup,
  Select,
  Text,
} from "@chakra-ui/react";
import {
  FieldErrors,
  UseFormClearErrors,
  UseFormRegister,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import { CheckoutFormDto } from "./types";
import InputMask from "react-input-mask";
import { getInstallmentValue } from "@/utils/paymentUtil";

type Props = {
  watch: UseFormWatch<CheckoutFormDto>;
  register: UseFormRegister<CheckoutFormDto>;
  setValue: UseFormSetValue<CheckoutFormDto>;
  errors: FieldErrors<CheckoutFormDto>;
  clearErrors: UseFormClearErrors<CheckoutFormDto>;
  orderTotal?: number;
};

const INVALID_CHARS = /[^0-9]/g;

export const PaymentForm = ({
  watch,
  register,
  setValue,
  errors,
  clearErrors,
  orderTotal,
}: Props) => {
  const paymentContact = watch("paymentContact");
  const paymentCpf = watch("paymentCpf");
  const paymentType = watch("paymentType");

  const contactWithoutMask = (contact: string) =>
    contact
      .replaceAll("(", "")
      .replaceAll(")", "")
      .replaceAll(" ", "")
      .replaceAll("-", "")
      .replaceAll("_", "");

  const setCorrectValue = (target: HTMLInputElement) => {
    const treatedTarget = target;
    const removedDotOrComma = treatedTarget.value.replace(INVALID_CHARS, "");
    treatedTarget.value = String(removedDotOrComma);
  };

  return (
    <Box
      bgColor="neutral.0"
      borderRadius={7}
      w="100%"
      border="solid 1px"
      borderColor="neutral.10"
      p={5}
    >
      <Text
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="700"
        fontSize={{ base: "16px", md: "18px" }}
        lineHeight="24px"
        color="secondary.300"
        mb={3}
      >
        Informações pessoais
      </Text>
      <Text
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="400"
        fontSize={{ base: "10px", md: "12px" }}
        lineHeight="18px"
        color="neutral.500"
        mb={6}
      >
        Essas informações são necessárias para credibilidade do pagamento
        perante aos bancos
      </Text>
      <Grid
        templateColumns={{
          base: "repeat(1, 1fr)",
          md: "repeat(1, 1fr)",
          lg: "repeat(3, 1fr)",
        }}
        gap={4}
        w="100%"
      >
        <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
          <FormControl w="100%">
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize={{ base: "10px", md: "12px" }}
              lineHeight="18px"
              color="neutral.500"
            >
              Nome completo do comprador*
            </Text>
            <Input
              type="text"
              w="100%"
              h={10}
              borderRadius={4}
              border={`1px solid ${
                errors?.paymentBuyerName?.message ? "red" : "#EAEAEA"
              }`}
              bgColor="neutral.0"
              {...register("paymentBuyerName")}
            />
            {errors?.paymentBuyerName?.message && (
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="500"
                fontSize="11px"
                lineHeight="18px"
                color="fail.500"
              >
                {errors.paymentBuyerName.message}
              </Text>
            )}
          </FormControl>
        </GridItem>
        <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
          <FormControl w="100%">
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize={{ base: "10px", md: "12px" }}
              lineHeight="18px"
              color="neutral.500"
            >
              Telefone*
            </Text>
            <Input
              type="text"
              as={InputMask}
              mask="(**) *****-****"
              w="100%"
              h={10}
              borderRadius={4}
              border={`1px solid ${
                errors?.paymentContact?.message ? "red" : "#EAEAEA"
              }`}
              bgColor="neutral.0"
              value={paymentContact}
              onChange={(event) => {
                clearErrors("paymentContact");
                const withoutMask = contactWithoutMask(event.target.value);

                const isValidNumberValue = /^[0-9]*$/.test(withoutMask);
                const validatedValue = isValidNumberValue
                  ? withoutMask
                  : paymentContact;

                setValue("paymentContact", validatedValue);
              }}
            />
            {errors?.paymentContact?.message && (
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="500"
                fontSize="11px"
                lineHeight="18px"
                color="fail.500"
              >
                {errors.paymentContact.message}
              </Text>
            )}
          </FormControl>
        </GridItem>
        <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
          <FormControl w="100%">
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize={{ base: "10px", md: "12px" }}
              lineHeight="18px"
              color="neutral.500"
            >
              CPF*
            </Text>
            <Input
              type="text"
              as={InputMask}
              mask="***.***.***-**"
              w="100%"
              h={10}
              borderRadius={4}
              border={`1px solid ${
                errors?.paymentCpf?.message ? "red" : "#EAEAEA"
              }`}
              bgColor="neutral.0"
              value={paymentCpf}
              onChange={(event) => {
                clearErrors("paymentCpf");

                const value = event.target.value
                  .replaceAll(".", "")
                  .replaceAll("-", "")
                  .replaceAll("_", "");

                const isValidNumberValue = /^[0-9]*$/.test(value);
                const validatedValue = isValidNumberValue ? value : paymentCpf;

                setValue("paymentCpf", validatedValue);
              }}
            />
            {errors?.paymentCpf?.message && (
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="500"
                fontSize="11px"
                lineHeight="18px"
                color="fail.500"
              >
                {errors.paymentCpf.message}
              </Text>
            )}
          </FormControl>
        </GridItem>
        <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
          <FormControl w="100%">
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize={{ base: "10px", md: "12px" }}
              lineHeight="18px"
              color="neutral.500"
            >
              Data de nascimento
            </Text>
            <Input
              type="date"
              w="100%"
              h={10}
              borderRadius={4}
              border={`1px solid ${
                errors?.paymentBirth?.message ? "red" : "#EAEAEA"
              }`}
              bgColor="neutral.0"
              {...register("paymentBirth")}
            />
            {errors?.paymentBirth?.message && (
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="500"
                fontSize="11px"
                lineHeight="18px"
                color="fail.500"
              >
                {errors.paymentBirth.message}
              </Text>
            )}
          </FormControl>
        </GridItem>
        <GridItem colSpan={{ base: 3, md: 3, lg: 2 }}>
          <RadioGroup w="100%" defaultValue={paymentType}>
            <HStack spacing={4}>
              <FormControl w={{ base: "40%", md: "50%", lg: "50%" }}>
                <HStack
                  mt="18px"
                  w="100%"
                  h={10}
                  borderRadius={4}
                  bgColor="neutral.0"
                  border={`1px solid ${
                    errors?.paymentType?.message ? "red" : "#EAEAEA"
                  }`}
                  pl={4}
                >
                  <Radio
                    w="100%"
                    size="lg"
                    value="pix"
                    onChange={(event) => {
                      clearErrors("cardNumber");
                      clearErrors("expirationDate");
                      clearErrors("secretNumber");
                      clearErrors("installment");
                      setValue(
                        "paymentType",
                        event.target.value as "pix" | "credit_card"
                      );
                    }}
                  >
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="600"
                      fontSize={{ base: "12px", md: "14px" }}
                      lineHeight="18px"
                      color="neutral.500"
                    >
                      PIX
                    </Text>
                  </Radio>
                </HStack>
                {errors?.paymentType?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.paymentType.message}
                  </Text>
                )}
              </FormControl>
              <FormControl w={{ base: "60%", md: "50%", lg: "50%" }}>
                <HStack
                  mt="18px"
                  w="100%"
                  h={10}
                  borderRadius={4}
                  bgColor="neutral.0"
                  border={`1px solid ${
                    errors?.paymentType?.message ? "red" : "#EAEAEA"
                  }`}
                  pl={4}
                >
                  <Radio
                    w="100%"
                    size="lg"
                    value="credit_card"
                    onChange={(event) => {
                      clearErrors("cardNumber");
                      clearErrors("expirationDate");
                      clearErrors("secretNumber");
                      clearErrors("installment");
                      setValue(
                        "paymentType",
                        event.target.value as "pix" | "credit_card"
                      );
                    }}
                  >
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="600"
                      fontSize={{ base: "12px", md: "14px" }}
                      lineHeight="18px"
                      color="neutral.500"
                    >
                      Cartão de crédito
                    </Text>
                  </Radio>
                </HStack>
                {errors?.paymentType?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.paymentType.message}
                  </Text>
                )}
              </FormControl>
            </HStack>
          </RadioGroup>
        </GridItem>
        <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
          <FormControl w="100%">
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize={{ base: "10px", md: "12px" }}
              lineHeight="18px"
              color="neutral.500"
            >
              Número do cartão
            </Text>
            <Input
              as={InputMask}
              mask="**** **** **** ****"
              type="text"
              w="100%"
              h={10}
              borderRadius={4}
              border={`1px solid ${
                errors?.cardNumber?.message ? "red" : "#EAEAEA"
              }`}
              bgColor="neutral.0"
              onInput={(event) =>
                setCorrectValue(event.target as HTMLInputElement)
              }
              onChange={(event) => {
                clearErrors("cardNumber");
                setValue("cardNumber", event.target.value);
              }}
              disabled={paymentType !== "credit_card"}
            />
            {errors?.cardNumber?.message && (
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="500"
                fontSize="11px"
                lineHeight="18px"
                color="fail.500"
              >
                {errors.cardNumber.message}
              </Text>
            )}
          </FormControl>
        </GridItem>
        <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
          <FormControl w="100%">
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize={{ base: "10px", md: "12px" }}
              lineHeight="18px"
              color="neutral.500"
            >
              Data de expiração
            </Text>
            <Input
              as={InputMask}
              mask="**/**"
              type="text"
              w="100%"
              h={10}
              borderRadius={4}
              border={`1px solid ${
                errors?.expirationDate?.message ? "red" : "#EAEAEA"
              }`}
              bgColor="neutral.0"
              onInput={(event) =>
                setCorrectValue(event.target as HTMLInputElement)
              }
              onChange={(event) => {
                clearErrors("expirationDate");
                setValue("expirationDate", event.target.value);
              }}
              disabled={paymentType !== "credit_card"}
            />
            {errors?.expirationDate?.message && (
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="500"
                fontSize="11px"
                lineHeight="18px"
                color="fail.500"
              >
                {errors.expirationDate.message}
              </Text>
            )}
          </FormControl>
        </GridItem>
        <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
          <FormControl w="100%">
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize={{ base: "10px", md: "12px" }}
              lineHeight="18px"
              color="neutral.500"
            >
              CVC/CVV
            </Text>
            <Input
              as={InputMask}
              mask="***"
              type="text"
              w="100%"
              h={10}
              borderRadius={4}
              border={`1px solid ${
                errors?.secretNumber?.message ? "red" : "#EAEAEA"
              }`}
              bgColor="neutral.0"
              onInput={(event) =>
                setCorrectValue(event.target as HTMLInputElement)
              }
              onChange={(event) => {
                clearErrors("secretNumber");
                setValue("secretNumber", event.target.value);
              }}
              disabled={paymentType !== "credit_card"}
            />
            {errors?.secretNumber?.message && (
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="500"
                fontSize="11px"
                lineHeight="18px"
                color="fail.500"
              >
                {errors.secretNumber.message}
              </Text>
            )}
          </FormControl>
        </GridItem>
        <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
          <FormControl w="100%">
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize={{ base: "10px", md: "12px" }}
              lineHeight="18px"
              color="neutral.500"
            >
              Parcelamento
            </Text>
            <Select
              h={10}
              borderRadius={4}
              {...register("installment")}
              disabled={paymentType !== "credit_card"}
            >
              <option value={1}>
                {getInstallmentValue(1, 3.5, orderTotal)}
              </option>
              <option value={2}>
                {getInstallmentValue(2, 5.5, orderTotal)}
              </option>
              <option value={3}>
                {getInstallmentValue(3, 6.5, orderTotal)}
              </option>
              <option value={4}>{getInstallmentValue(4, 7, orderTotal)}</option>
              <option value={5}>{getInstallmentValue(5, 8, orderTotal)}</option>
              <option value={6}>
                {getInstallmentValue(6, 8.5, orderTotal)}
              </option>
              <option value={7}>
                {getInstallmentValue(7, 9.5, orderTotal)}
              </option>
              <option value={8}>
                {getInstallmentValue(8, 10.5, orderTotal)}
              </option>
              <option value={9}>
                {getInstallmentValue(9, 11, orderTotal)}
              </option>
              <option value={10}>
                {getInstallmentValue(10, 12, orderTotal)}
              </option>
              <option value={11}>
                {getInstallmentValue(11, 13, orderTotal)}
              </option>
              <option value={12}>
                {getInstallmentValue(12, 13.5, orderTotal)}
              </option>
            </Select>
            {errors?.installment?.message && (
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="500"
                fontSize="11px"
                lineHeight="18px"
                color="fail.500"
              >
                {errors.installment.message}
              </Text>
            )}
          </FormControl>
        </GridItem>
      </Grid>
      <HStack w="100%" justifyContent="center" mt={10}>
        <Button
          variant="ghost"
          bgColor="secondary.500"
          type="submit"
          _hover={{
            bgColor: "secondary.300",
          }}
          _active={{
            bgColor: "secondary.500",
          }}
        >
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="400"
            fontSize="16px"
            lineHeight="20px"
            color="neutral.0"
          >
            Realizar pagamento
          </Text>
        </Button>
      </HStack>
    </Box>
  );
};
