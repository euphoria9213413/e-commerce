import { SearchIcon } from "@/components/icons/search";
import {
  Button,
  Grid,
  GridItem,
  HStack,
  Input,
  InputGroup,
  InputLeftElement,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Radio,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useRadioGroup,
} from "@chakra-ui/react";
import * as Font from "@/components/texts";
import * as T from "@/components/table";
import {
  CheckoutFormDataDto,
  CheckoutFormDto,
  GraduateIndexDto,
  GraduateIndexFilterDto,
} from "./types";
import { UseFormClearErrors } from "react-hook-form";

type Props = {
  isOpen: boolean;
  onClose: () => void;
  handleInputChange: () => void;
  filter: GraduateIndexFilterDto;
  setFilter: (value: GraduateIndexFilterDto) => void;
  graduates: GraduateIndexDto[];
  saleIndex: number;
  sale: CheckoutFormDataDto;
  clearErrors: UseFormClearErrors<CheckoutFormDto>;
  setSaleData: (newSale: CheckoutFormDataDto) => void;
};

/* eslint-disable react/no-children-prop */
export const GraduateSearchModal = ({
  isOpen,
  onClose,
  handleInputChange,
  filter,
  setFilter,
  graduates,
  saleIndex,
  sale,
  clearErrors,
  setSaleData,
}: Props) => {
  const { getRadioProps, getRootProps, setValue, value } = useRadioGroup();

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered
      scrollBehavior="inside"
      {...getRootProps()}
    >
      <ModalOverlay />
      <ModalContent
        bgColor="neutral.200"
        w="100%"
        minW={{ base: "80%", md: "80%", lg: "700px" }}
        maxW={{ base: "95%", md: "95%", lg: "1000px" }}
      >
        <ModalHeader>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="700"
            fontSize={{ base: "15px", md: "18px" }}
            lineHeight="24px"
            color="secondary.500"
          >
            Selecione um formando
          </Text>
        </ModalHeader>
        <ModalBody maxH="500px">
          <InputGroup w="100%" alignItems="center">
            <InputLeftElement
              justifyContent="center"
              pointerEvents="none"
              children={<SearchIcon />}
              top=""
            />
            <Input
              type="text"
              placeholder="Buscar por nome do formando"
              _placeholder={{ color: "neutral.50" }}
              w="100%"
              h={9}
              borderRadius={4}
              textIndent="10px"
              border="1px solid #EAEAEA"
              bgColor="neutral.0"
              value={filter.name}
              onChange={(event) => {
                setFilter({ name: event.target.value });
                handleInputChange();
              }}
            />
          </InputGroup>
          <TableContainer>
            <Table>
              <Thead>
                <Tr>
                  <Th></Th>
                  <Th textAlign="center">
                    <Font.TableTheadText>Nome</Font.TableTheadText>
                  </Th>
                  <Th textAlign="center">
                    <Font.TableTheadText>Turma</Font.TableTheadText>
                  </Th>
                </Tr>
              </Thead>
              <Tbody>
                {graduates.map((graduate) => (
                  <T.Body key={graduate.id}>
                    <Td textAlign="center">
                      <Radio
                        w="100%"
                        size={{ base: "md", md: "lg" }}
                        key={graduate.id}
                        {...getRadioProps({
                          value: graduate.id,
                        })}
                        onChange={(event) =>
                          setValue(Number(event.target.value))
                        }
                      />
                    </Td>
                    <Td
                      textAlign="center"
                      wordBreak="break-all"
                      whiteSpace="pre-wrap"
                    >
                      <Font.TableBodyText>{graduate.name}</Font.TableBodyText>
                    </Td>
                    <Td
                      textAlign="center"
                      wordBreak="break-all"
                      whiteSpace="pre-wrap"
                    >
                      <Font.TableBodyText>{graduate.class}</Font.TableBodyText>
                    </Td>
                  </T.Body>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
        </ModalBody>
        <ModalFooter>
          <Grid
            w="100%"
            templateColumns={{
              base: "repeat(1, 1fr)",
              md: "repeat(2, 1fr)",
              lg: "repeat(2, 1fr)",
            }}
            gap={5}
          >
            <GridItem>
              <HStack
                justifyContent={{
                  base: "center",
                  md: "flex-end",
                }}
              >
                <Button
                  variant="ghost"
                  bgColor="neutral.0"
                  onClick={onClose}
                  border="solid 1px"
                  borderColor="secondary.500"
                  w={{ base: "100%", md: "150px" }}
                  _hover={{
                    bgColor: "secondary.100",
                  }}
                  _active={{
                    bgColor: "secondary.300",
                  }}
                >
                  <Text
                    fontFamily="Inter"
                    fontStyle="normal"
                    fontWeight="600"
                    fontSize="16px"
                    lineHeight="20px"
                    color="secondary.500"
                  >
                    Cancelar
                  </Text>
                </Button>
              </HStack>
            </GridItem>
            <GridItem>
              <HStack
                justifyContent={{
                  base: "center",
                  md: "flex-start",
                }}
              >
                <Button
                  variant="ghost"
                  bgColor="secondary.500"
                  w={{ base: "100%", md: "150px" }}
                  _hover={{
                    bgColor: "secondary.300",
                  }}
                  _active={{
                    bgColor: "secondary.500",
                  }}
                  onClick={() => {
                    clearErrors(`sales.${saleIndex}.buyerContact`);

                    const graduateName = graduates.find(
                      (graduate) => graduate.id === value
                    )!.name;

                    const newSale = {
                      ...sale,
                      graduateId: Number(value),
                      graduateName: graduateName,
                    };

                    setSaleData(newSale);
                    onClose();
                  }}
                >
                  <Text
                    fontFamily="Inter"
                    fontStyle="normal"
                    fontWeight="400"
                    fontSize="16px"
                    lineHeight="20px"
                    color="neutral.0"
                  >
                    Confirmar
                  </Text>
                </Button>
              </HStack>
            </GridItem>
          </Grid>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
