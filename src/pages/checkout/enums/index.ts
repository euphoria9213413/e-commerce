export enum FeePolicyEnum {
  absorver = "absorver",
  repassar = "repassar",
}

export enum PaymentProcessorMessage {
  approved = "APPROVED",
  declined = "DECLINED",
}
