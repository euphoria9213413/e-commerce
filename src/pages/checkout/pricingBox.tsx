import { Separator } from "@/components/separator";
import { Box, HStack, Stack, Text, VStack } from "@chakra-ui/react";
import { Product, StoreEventDto } from "../home/types";
import { moneyFormat } from "@/utils/formatUtil";
import { useCheckout } from "@/hooks/useCheckout";
import { useCallback } from "react";
import { CheckoutDataDto } from "./types";

type Props = Pick<StoreEventDto, "products">;

export const PricingBox = ({ products }: Props) => {
  const { getCheckout, setCheckout } = useCheckout();
  const checkoutItems: CheckoutDataDto = JSON.parse(getCheckout() || "");

  const productAmount = useCallback((product: Product) => {
    const quantity = Object.entries(checkoutItems.items).find(
      (item) => item[0] === product.name
    )![1];

    return Number(product.unitaryValue) * Number(quantity);
  }, []);

  const productQuantity = useCallback((product: Product) => {
    return Object.entries(checkoutItems.items).find(
      (item) => item[0] === product.name
    )![1];
  }, []);

  const getOrderTotal = useCallback(() => {
    const orderTotal = Object.entries(checkoutItems.items).reduce(
      (acc, curr) => {
        const name = curr[0];
        const quantity = curr[1];
        const productValue = products.find(
          (product) => product.name === name
        )!.unitaryValue;

        acc += Number(productValue) * Number(quantity);
        return acc;
      },
      0
    );

    setCheckout({ ...checkoutItems, orderTotal });

    return orderTotal;
  }, []);

  return (
    <Box
      bgColor="neutral.0"
      w={{ base: "100%", md: "100%", lg: "400px" }}
      p="20px 15px"
      borderRadius={7}
      border="solid 1px"
      borderColor="neutral.10"
    >
      <Text
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="700"
        fontSize={{ base: "16px", md: "18px" }}
        lineHeight="24px"
        color="secondary.300"
      >
        Meus produtos
      </Text>
      <Stack spacing={6}>
        {products.map((product) => (
          <HStack key={product.name} justifyContent="space-between">
            <Box>
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="600"
                fontSize={{ base: "17px", md: "18px" }}
                lineHeight="30px"
                color="neutral.50"
              >
                {`Produto ${product.name}`}
              </Text>
              <HStack>
                <Text
                  fontFamily="Inter"
                  fontStyle="normal"
                  fontWeight="600"
                  fontSize={{ base: "14px", md: "16px" }}
                  lineHeight="30px"
                  color="neutral.50"
                >
                  {moneyFormat(Number(product.unitaryValue))}
                </Text>
                <Text
                  fontFamily="Inter"
                  fontStyle="normal"
                  fontWeight="600"
                  fontSize={{ base: "10px", md: "12px" }}
                  lineHeight="30px"
                  color="neutral.50"
                >
                  un.
                </Text>
              </HStack>
            </Box>
            <VStack alignItems="flex-end">
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="700"
                fontSize={{ base: "16px", md: "18px" }}
                lineHeight="20px"
                color="neutral.50"
              >
                {moneyFormat(productAmount(product))}
              </Text>
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="600"
                fontSize={{ base: "12px", md: "14px" }}
                lineHeight="30px"
                color="neutral.50"
              >
                {`(${productQuantity(product)} un.)`}
              </Text>
            </VStack>
          </HStack>
        ))}
      </Stack>
      <Separator m="15px 0" />
      <HStack justifyContent="space-between">
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="400"
          fontSize={{ base: "14px", md: "16px" }}
          lineHeight="20px"
          color="neutral.600"
        >
          Total do pedido
        </Text>
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="700"
          fontSize={{ base: "16px", md: "18px" }}
          lineHeight="20px"
          color="secondary.500"
        >
          {moneyFormat(getOrderTotal())}
        </Text>
      </HStack>
      <HStack w="100%" justifyContent="flex-end">
        <HStack w="70%" justifyContent="flex-end">
          <Text
            textAlign="right"
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="400"
            fontSize={{ base: "10px", md: "11px" }}
            lineHeight="14px"
            color="neutral.50"
          >
            *O pagamento no cartão de crédito pode estar sujeito às taxas do
            cartão.
          </Text>
        </HStack>
      </HStack>
    </Box>
  );
};
