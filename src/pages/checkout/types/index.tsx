export type CheckoutDataDto = {
  items: {
    [key: string]: number;
  };
  orderTotal?: number;
  pixCopyPasteValue?: string;
  expirationDate?: string;
  expirationHour?: string;
  desktopBannerImageUrl?: string;
  mobileBannerImageUrl?: string;
};

export type GraduateIndexQueryDto = {
  storeId: number;
  name?: string;
};

export type GraduateIndexDto = {
  id: number;
  code: string;
  name: string;
  cpf: string;
  class: string;
  situation: boolean;
};

export type GraduateIndexPaginatedDto = {
  data: GraduateIndexDto[];
  page: number;
  perPage: number;
  lastPage: number;
  total: number;
};

export type GraduateIndexFilterDto = {
  name: string;
};

export type CheckoutFormDataDto = {
  index: number;
  productId: number;
  graduateId?: number;
  graduateName?: string;
  buyerName?: string;
  buyerEmail?: string;
  buyerContact?: string;
};

export type CheckoutFormDto = {
  sales: CheckoutFormDataDto[];
  paymentBuyerName: string;
  paymentContact: string;
  paymentCpf: string;
  paymentBirth?: string;
  paymentType: "pix" | "credit_card";
  cardNumber?: string;
  expirationDate?: string;
  secretNumber?: string;
  installment?: number;
};

export type SalePixCopyPaste = {
  pixCopyPasteValue: string;
  expirationDate: string;
  expirationHour: string;
};
