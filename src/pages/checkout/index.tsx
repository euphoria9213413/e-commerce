import { Container } from "@/components/container";
import { Footer } from "@/components/footer";
import { Header } from "@/components/header";
import { Box, Image, Stack, useBreakpointValue } from "@chakra-ui/react";
import { Content } from "./content";
import LoadingOverlay from "@/components/overlay";
import { useNavigate, useParams } from "react-router-dom";
import { useCustomToast } from "@/hooks/useToast";
import { useEffect, useState } from "react";
import { StoreEventDto } from "../home/types";
import { useStoreEvent } from "@/hooks/useStore";
import { useUser } from "@/hooks/useUser";
import { UserDataDto } from "../profile/types";
import { useCheckout } from "@/hooks/useCheckout";

export const Checkout = () => {
  const { slug } = useParams();
  const { getUser } = useUser();
  const { getCheckout } = useCheckout();
  const { getStoreEventBySlug } = useStoreEvent();
  const { customToast } = useCustomToast();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [user, setUser] = useState<UserDataDto | undefined>();
  const [event, setEvent] = useState<StoreEventDto | undefined>();

  const isMobile = useBreakpointValue({ base: true, md: false });
  const navigate = useNavigate();

  useEffect(() => {
    setIsLoading(true);
    if (!getCheckout()) return navigate("/");

    Promise.all([getUser(), getStoreEventBySlug(slug!)])
      .then((responses) => {
        setUser(responses[0].data);
        setEvent(responses[1].data);
      })
      .catch(() => {
        customToast("error", "Falha ao carregar evento");
        navigate("/");
      })
      .finally(() => setIsLoading(false));
  }, []);

  if (!user || !event) {
    return (
      <Stack minH="100vh" w="100%" position="relative">
        <LoadingOverlay />
        <Header />
        <Footer />
      </Stack>
    );
  }

  return (
    <Stack minH="100vh" w="100%" position="relative">
      <Header />
      {isLoading && <LoadingOverlay />}
      <Image
        src={isMobile ? event.mobileMediaUrl : event.desktopMediaUrl}
        maxH={{ base: "250px", md: "472px" }}
        mt={1}
      ></Image>
      <Box bgColor="secondary.500" width="100%" h={6} mt={2}></Box>
      <Container>
        <Content user={user} event={event} setIsLoading={setIsLoading} />
      </Container>
      <Footer />
    </Stack>
  );
};
