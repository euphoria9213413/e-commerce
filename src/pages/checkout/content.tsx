import { Box, Grid, GridItem } from "@chakra-ui/react";
import {
  FieldErrors,
  SubmitHandler,
  UseFormClearErrors,
  UseFormSetValue,
  UseFormWatch,
  useForm,
} from "react-hook-form";
import { DataForm } from "./dataForm";
import { Product, StoreEventDto } from "../home/types";
import { useCheckout } from "@/hooks/useCheckout";
import { useCallback, useEffect, useMemo, useState } from "react";
import { PricingBox } from "./pricingBox";
import { PaymentForm } from "./paymentForm";
import {
  CheckoutDataDto,
  CheckoutFormDataDto,
  CheckoutFormDto,
  GraduateIndexDto,
  GraduateIndexFilterDto,
} from "./types";
import { useCustomToast } from "@/hooks/useToast";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import { UserDataDto } from "../profile/types";
import { tokenGenerate } from "@/utils/tokenUtil";
import { useNavigate } from "react-router-dom";

type FormListProps = {
  product: Product;
  watch: UseFormWatch<CheckoutFormDto>;
  setValue: UseFormSetValue<CheckoutFormDto>;
  errors: FieldErrors<CheckoutFormDto>;
  graduates: GraduateIndexDto[];
  handleInputChange: () => void;
  filter: GraduateIndexFilterDto;
  setFilter: (value: GraduateIndexFilterDto) => void;
  clearErrors: UseFormClearErrors<CheckoutFormDto>;
};

export const FormList = ({
  product,
  watch,
  setValue,
  errors,
  graduates,
  handleInputChange,
  filter,
  setFilter,
  clearErrors,
}: FormListProps) => {
  const { getCheckout } = useCheckout();

  const quantity = useMemo(() => {
    const checkoutItems: CheckoutDataDto = JSON.parse(getCheckout() || "");

    return Number(
      Object.entries(checkoutItems.items).find(
        (item) => item[0] === product.name
      )![1]
    );
  }, []);

  const items = [];

  for (let i = 1; i <= quantity; i++) {
    items.push(
      <Box key={i}>
        <DataForm
          graduates={graduates}
          handleInputChange={handleInputChange}
          filter={filter}
          setFilter={setFilter}
          product={product}
          quantity={quantity}
          index={i}
          watch={watch}
          setValue={setValue}
          errors={errors}
          clearErrors={clearErrors}
        />
      </Box>
    );
  }

  return items;
};

const schema = Joi.object({
  paymentBuyerName: Joi.string().required().messages({
    "string.empty": "Nome completo do comprador deve ser informado",
  }),
  paymentContact: Joi.string().required().messages({
    "any.required": "Telefone deve ser informado",
    "string.empty": "Telefone deve ser informado",
  }),
  paymentCpf: Joi.string().required().messages({
    "any.required": "CPF deve ser informado",
    "string.empty": "CPF deve ser informado",
  }),
  paymentBirth: Joi.string().allow(null, ""),
  paymentType: Joi.string().valid("pix", "credit_card").required().messages({
    "any.only": "Forma de pagamento deve ser informada.",
  }),
  cardNumber: Joi.string().when("paymentType", {
    is: "credit_card",
    then: Joi.string()
      .required()
      .custom((value, helper) => {
        const valueReplaced = value.replace(/[^0-9]/g, "");

        if (valueReplaced.length !== 16) {
          return helper.error("string.invalid");
        }

        return value;
      })
      .messages({
        "any.required": "Número do cartão deve ser informado",
        "string.empty": "Número do cartão deve ser informado",
        "string.invalid": "Número do cartão inválido",
      }),
    otherwise: Joi.allow("", null),
  }),
  expirationDate: Joi.string().when("paymentType", {
    is: "credit_card",
    then: Joi.string()
      .required()
      .custom((value, helper) => {
        const valueReplaced = value.replace(/[^0-9]/g, "");

        if (valueReplaced.length !== 4) {
          return helper.error("string.invalid");
        }

        return value;
      })
      .messages({
        "any.required": "Data de expiração deve ser informado",
        "string.empty": "Data de expiração deve ser informado",
        "string.invalid": "Data de expiração inválida",
      }),
    otherwise: Joi.allow("", null),
  }),
  secretNumber: Joi.string().when("paymentType", {
    is: "credit_card",
    then: Joi.string()
      .required()
      .custom((value, helper) => {
        const valueReplaced = value.replace(/[^0-9]/g, "");

        if (valueReplaced.length !== 3) {
          return helper.error("string.invalid");
        }

        return value;
      })
      .messages({
        "any.required": "CVC/CVV deve ser informado",
        "string.empty": "CVC/CVV deve ser informado",
        "string.invalid": "CVC/CVV inválido",
      }),
    otherwise: Joi.allow("", null),
  }),
  installment: Joi.number().when("paymentType", {
    is: "credit_card",
    then: Joi.number().required().messages({
      "any.required": "Parcelamento deve ser informado",
    }),
    otherwise: Joi.allow("", null),
  }),
  sales: Joi.array()
    .items(
      Joi.object({
        index: Joi.number().allow(null, ""),
        productId: Joi.number().required().messages({
          "any.required": "productId deve ser informada",
          "string.empty": "productId deve ser informada",
        }),
        graduateId: Joi.number().required().messages({
          "any.required": "graduateId deve ser informada",
          "string.empty": "graduateId deve ser informada",
        }),
        graduateName: Joi.string().required().messages({
          "any.required": "Nome completo do formando deve ser informado",
          "string.empty": "Nome completo do formando deve ser informado",
        }),
        buyerName: Joi.string().required().messages({
          "any.required": "Nome completo do comprador deve ser informado",
          "string.empty": "Nome completo do comprador deve ser informado",
        }),
        buyerEmail: Joi.string().required().messages({
          "any.required": "E-mail do comprador deve ser informado",
          "string.empty": "E-mail do comprador deve ser informado",
        }),
        buyerContact: Joi.string().required().messages({
          "any.required": "Telefone do comprador deve ser informado",
          "string.empty": "Telefone do comprador deve ser informado",
        }),
      })
    )
    .required(),
});

type Props = {
  user: UserDataDto;
  event: StoreEventDto;
  setIsLoading: (value: boolean) => void;
};

const initialFilter: GraduateIndexFilterDto = {
  name: "",
};

export const Content = ({ user, event, setIsLoading }: Props) => {
  const { getGraduates } = useCheckout();
  const { getCheckout, storeSale, setCheckout } = useCheckout();
  const { customToast } = useCustomToast();
  const [graduates, setGraduates] = useState<GraduateIndexDto[]>([]);
  const [filter, setFilter] = useState<GraduateIndexFilterDto>(initialFilter);
  const [stopedTyping, setStopedTyping] = useState(false);
  const [typingTimer, setTypingTimer] = useState<NodeJS.Timeout | undefined>();

  const navigate = useNavigate();
  const checkoutItems: CheckoutDataDto = JSON.parse(getCheckout() || "");

  const handleInputChange = useCallback(() => {
    clearTimeout(typingTimer);

    const newTypingTimer = setTimeout(() => {
      setStopedTyping(true);
    }, 500);

    setTypingTimer(newTypingTimer);
  }, []);

  const callbackGraduates = useCallback(() => {
    setIsLoading(true);

    getGraduates({ storeId: Number(event.id), ...filter })
      .then((response) => {
        setGraduates(response.data.data);
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  }, [filter]);

  useEffect(() => callbackGraduates(), []);

  useEffect(() => {
    if (stopedTyping) {
      callbackGraduates();
      setStopedTyping(false);
    }
  }, [filter, stopedTyping]);

  const total = event.products.reduce((acc: CheckoutFormDataDto[], curr) => {
    const quantity = Object.entries(checkoutItems.items).find(
      (item) => item[0] === curr.name
    )![1];

    const items = [];
    for (let i = 1; i <= Number(quantity); i++) {
      const extra =
        i === 1
          ? {
              buyerName: user.name,
              buyerEmail: user.email,
              buyerContact: user.contact,
            }
          : {};

      const data = {
        index: i,
        productId: curr.id,
        ...extra,
      };

      items.push(data);
    }

    acc = [...acc, ...items];
    return acc;
  }, []);

  const {
    handleSubmit,
    setValue,
    register,
    watch,
    clearErrors,
    formState: { errors },
  } = useForm<CheckoutFormDto>({
    resolver: joiResolver(schema),
    defaultValues: {
      sales: total,
      paymentBuyerName: user.name,
      paymentContact: user.contact,
      paymentCpf: user.cpf,
      paymentBirth: user.birth,
      paymentType: "pix",
      installment: 1,
    },
  });

  const contactWithoutMask = (contact: string) =>
    contact
      .replaceAll("(", "")
      .replaceAll(")", "")
      .replaceAll(" ", "")
      .replaceAll("-", "")
      .replaceAll("_", "");

  const onSubmit: SubmitHandler<CheckoutFormDto> = async (value) => {
    setIsLoading(true);

    const salesData = value.sales.map((sale) => ({
      ...sale,
      buyerContact: sale.buyerContact
        ? contactWithoutMask(sale.buyerContact)
        : "",
    }));

    const data: CheckoutFormDto = {
      ...value,
      sales: salesData,
      paymentCpf: value.paymentCpf.replaceAll(".", "").replace("-", ""),
      paymentContact: value.paymentContact
        ? contactWithoutMask(value.paymentContact)
        : "",
      paymentBirth: value.paymentBirth || undefined,
      cardNumber: value.cardNumber
        ? await tokenGenerate(value.cardNumber.replaceAll(" ", ""))
        : "",
      expirationDate: value.expirationDate
        ? await tokenGenerate(value.expirationDate.replaceAll(" ", ""))
        : "",
      secretNumber: value.secretNumber
        ? await tokenGenerate(value.secretNumber.replaceAll(" ", ""))
        : "",
    };

    storeSale(data)
      .then((response) => {
        customToast("success", "Pedido realizado com sucesso");
        if (data.paymentType === "pix") {
          setCheckout({
            ...checkoutItems,
            pixCopyPasteValue: response.data.pixCopyPasteValue,
            expirationDate: response.data.expirationDate,
            expirationHour: response.data.expirationHour,
            desktopBannerImageUrl: event.desktopMediaUrl,
            mobileBannerImageUrl: event.mobileMediaUrl,
          });
          navigate("/pagamento");

          return;
        }

        setCheckout({
          ...checkoutItems,
          desktopBannerImageUrl: event.desktopMediaUrl!,
          mobileBannerImageUrl: event.mobileMediaUrl!,
        });

        navigate("/pagamento-aprovado");
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  return (
    <Grid
      w="100%"
      m="10px 0"
      templateColumns={{
        base: "repeat(1, 1fr)",
        md: "repeat(5, 1fr)",
        lg: "repeat(5, 1fr)",
      }}
      as="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <GridItem w="100%" colSpan={{ base: 5, md: 5, lg: 4 }}>
        {event.products.map((product) => (
          <Box key={product.name}>
            <FormList
              graduates={graduates}
              handleInputChange={handleInputChange}
              watch={watch}
              filter={filter}
              setFilter={setFilter}
              product={product}
              setValue={setValue}
              errors={errors}
              clearErrors={clearErrors}
            />
          </Box>
        ))}
      </GridItem>
      <GridItem
        colSpan={{ base: 5, md: 5, lg: 1 }}
        m={{ base: "0 0 20px 0", md: "0 0 20px 0", lg: "0 0 0 20px" }}
      >
        <PricingBox products={event.products} />
      </GridItem>
      <GridItem w="100%" colSpan={{ base: 5, md: 5, lg: 4 }}>
        <PaymentForm
          watch={watch}
          setValue={setValue}
          register={register}
          errors={errors}
          clearErrors={clearErrors}
          orderTotal={checkoutItems.orderTotal}
        />
      </GridItem>
    </Grid>
  );
};
