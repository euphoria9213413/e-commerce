import {
  Button,
  Flex,
  FormControl,
  Input,
  Stack,
  Text,
  VStack,
} from "@chakra-ui/react";
import { SubmitHandler, useForm } from "react-hook-form";
import { joiResolver } from "@hookform/resolvers/joi";
import Joi from "joi";
import { useNavigate, useParams } from "react-router-dom";
import { useState } from "react";
import { useCustomToast } from "@/hooks/useToast";
import { useLogin } from "@/hooks/useLogin";
import { UpdateAccountPasswordDto } from "./types/newPassword";
import { AccountBox } from "../../components/accountBox";
import { Header } from "@/components/header";
import { Footer } from "@/components/footer";

const schema = Joi.object({
  newPassword: Joi.string().required().messages({
    "string.empty": "Nova senha deve ser informada",
  }),
  newPasswordConfirmation: Joi.string()
    .required()
    .equal(Joi.ref("newPassword"))
    .messages({
      "string.empty": "Confirmação de nova senha deve ser informada",
      "any.only": "Confirmação de nova senha não confere com a senha",
    }),
});

export const NewPassword = () => {
  const { token } = useParams();
  const navigate = useNavigate();
  const { doNewPassword } = useLogin();
  const { customToast } = useCustomToast();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UpdateAccountPasswordDto>({
    resolver: joiResolver(schema),
  });

  const onSubmit: SubmitHandler<UpdateAccountPasswordDto> = (props) => {
    setIsLoading(true);

    doNewPassword(props, token || "")
      .then(() => {
        customToast("success", "Senha redefinida com sucesso");
        navigate("/login");
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  return (
    <Stack bg="neutral.100" h="100vh" w="100%">
      <Header />
      <Flex
        h="100%"
        w="100%"
        justifyContent="center"
        alignItems="center"
        p={{ base: "0 20px", md: "0 0" }}
      >
        <AccountBox>
          <VStack
            w="100%"
            spacing={7}
            as="form"
            onSubmit={handleSubmit(onSubmit)}
          >
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize="30px"
              lineHeight="36px"
              color="neutral.400"
            >
              Alterar senha
            </Text>
            <VStack w="100%" spacing={7}>
              <FormControl w="100%">
                <Input
                  type="password"
                  placeholder="Nova senha"
                  _placeholder={{ color: "neutral.50" }}
                  w="100%"
                  h={50}
                  borderRadius="50px"
                  textIndent="15px"
                  border={`1px solid ${
                    errors?.newPassword?.message ? "red" : "#EAEAEA"
                  }`}
                  bgColor="neutral.0"
                  {...register("newPassword")}
                />
                {errors?.newPassword?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.newPassword.message}
                  </Text>
                )}
              </FormControl>
              <FormControl w="100%">
                <Input
                  type="password"
                  placeholder="Confirmar nova senha"
                  _placeholder={{ color: "neutral.50" }}
                  w="100%"
                  h={50}
                  borderRadius="50px"
                  textIndent="15px"
                  border={`1px solid ${
                    errors?.newPasswordConfirmation?.message ? "red" : "#EAEAEA"
                  }`}
                  bgColor="neutral.0"
                  {...register("newPasswordConfirmation")}
                />
                {errors?.newPasswordConfirmation?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.newPasswordConfirmation.message}
                  </Text>
                )}
              </FormControl>
            </VStack>
            <Button
              w="100%"
              borderRadius="50px"
              backgroundColor="neutral.700"
              textColor="neutral.0"
              h="50px"
              colorScheme="neutral.700"
              variant="solid"
              type="submit"
              isLoading={isLoading}
              _hover={{
                bgColor: "neutral.400",
              }}
              _active={{
                bgColor: "neutral.700",
              }}
            >
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="16px"
                lineHeight="24px"
                cursor="pointer"
              >
                entrar
              </Text>
            </Button>
          </VStack>
        </AccountBox>
      </Flex>
      <Footer />
    </Stack>
  );
};
