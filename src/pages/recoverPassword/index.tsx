import {
  Button,
  Flex,
  FormControl,
  HStack,
  Input,
  Stack,
  Text,
  VStack,
} from "@chakra-ui/react";
import { AccountBox } from "../../components/accountBox";
import { useNavigate } from "react-router-dom";
import { RecoverPasswordDto } from "./types";
import { joiResolver } from "@hookform/resolvers/joi";
import { SubmitHandler, useForm } from "react-hook-form";
import { useState } from "react";
import Joi from "joi";
import { useLogin } from "@/hooks/useLogin";
import { useCustomToast } from "@/hooks/useToast";
import { Header } from "@/components/header";
import { Footer } from "@/components/footer";
import { env } from "@/configs/env";

const schema = Joi.object({
  email: Joi.string().required().messages({
    "string.empty": "E-mail deve ser informado",
  }),
  link_to_redirect: Joi.string().required(),
});

export const RecoverPassword = () => {
  const navigate = useNavigate();
  const { doSendPasswordRecoverEmail } = useLogin();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { customToast } = useCustomToast();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<RecoverPasswordDto>({
    defaultValues: { email: "", link_to_redirect: env.main_uri },
    resolver: joiResolver(schema),
  });

  const onSubmit: SubmitHandler<RecoverPasswordDto> = (props) => {
    setIsLoading(true);

    doSendPasswordRecoverEmail(props)
      .then(() => {
        customToast(
          "success",
          "Solicitação completa. Aguarde o e-mail de alteração na sua caixa de entrada"
        );
        navigate("/login");
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  return (
    <Stack bg="neutral.100" h="100vh" w="100%">
      <Header />
      <Flex
        h="100%"
        w="100%"
        justifyContent="center"
        alignItems="center"
        p={{ base: "0 20px", md: "0 0" }}
      >
        <AccountBox>
          <VStack
            w="100%"
            spacing={7}
            as="form"
            onSubmit={handleSubmit(onSubmit)}
          >
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize="30px"
              lineHeight="36.31px"
              color="neutral.400"
            >
              Recuperar senha
            </Text>
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="14px"
              lineHeight="18px"
              color="neutral.50"
            >
              Inclua o e-mail cadastrado para que a nova senha seja enviada
            </Text>
            <VStack w="100%" spacing={7}>
              <FormControl w="100%">
                <Input
                  placeholder="E-mail"
                  bg="neutral.0"
                  borderRadius="50px"
                  h={50}
                  size="md"
                  type="email"
                  _placeholder={{ color: "neutral.50" }}
                  w="100%"
                  textIndent="15px"
                  border={`1px solid ${
                    errors?.email?.message ? "red" : "#EAEAEA"
                  }`}
                  bgColor="neutral.0"
                  {...register("email")}
                />
                {errors?.email?.message && (
                  <Text
                    fontFamily="Lato"
                    fontStyle="normal"
                    fontWeight="500"
                    fontSize="11px"
                    lineHeight="18px"
                    color="fail.500"
                  >
                    {errors.email.message}
                  </Text>
                )}
              </FormControl>
            </VStack>
            <Button
              w="100%"
              borderRadius="50px"
              backgroundColor="neutral.700"
              h="50px"
              colorScheme="neutral.700"
              variant="solid"
              type="submit"
              isLoading={isLoading}
              _hover={{
                bgColor: "neutral.400",
              }}
              _active={{
                bgColor: "neutral.700",
              }}
            >
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="16px"
                lineHeight="24px"
                cursor="pointer"
                onClick={() => navigate("/recuperar-senha")}
              >
                enviar
              </Text>
            </Button>

            <HStack w="100%" justifyContent="center">
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="15px"
                lineHeight="18px"
                color="secondary.600"
              >
                Não tem uma conta?
              </Text>
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="15px"
                lineHeight="18px"
                color="secondary.500"
                cursor="pointer"
                onClick={() => navigate("/criar-conta")}
              >
                Cadastre-se
              </Text>
            </HStack>
          </VStack>
        </AccountBox>
      </Flex>
      <Footer />
    </Stack>
  );
};
