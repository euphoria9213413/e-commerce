import { Stack, Text } from "@chakra-ui/react";

export const Description = () => {
  return (
    <Stack spacing={10}>
      <Text
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="700"
        fontSize={{ base: "25px", md: "30px" }}
        lineHeight="48px"
        color="secondary.500"
      >
        Pagamento Aprovado
      </Text>
      <Text
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="600"
        fontSize={{ base: "18px", md: "20px" }}
        lineHeight="20px"
        color="neutral.600"
      >
        Pedido realizado com sucesso!
      </Text>
      <Text
        fontFamily="Inter"
        fontStyle="normal"
        fontWeight="400"
        fontSize={{ base: "17px", md: "19px" }}
        lineHeight="20px"
        color="neutral.350"
      >
        {`Sua compra foi concluída! Em breve você receberá um e-mail contendo
          seus números da sorte. Você também poderá consultá-los em "Meus
          Pedidos".`}
      </Text>
    </Stack>
  );
};
