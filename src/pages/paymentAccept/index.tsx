import { Container } from "@/components/container";
import { Header } from "@/components/header";
import {
  Box,
  Button,
  Grid,
  GridItem,
  HStack,
  Image,
  Stack,
  Text,
  useBreakpointValue,
  useTimeout,
} from "@chakra-ui/react";
import { Footer } from "@/components/footer";
import { Description } from "./description";
import { PaymentCheckIcon } from "@/components/icons/paymentCheck";
import { useNavigate } from "react-router-dom";
import { useCheckout } from "@/hooks/useCheckout";
import { useEffect, useState } from "react";
import { CheckoutDataDto } from "../checkout/types";

export const PaymentAccept = () => {
  const navigate = useNavigate();
  const { getCheckout, removeCheckout } = useCheckout();
  const [orderTotal, setOrderTotal] = useState<number | undefined>(0);
  const checkout = getCheckout();
  const checkoutItems: CheckoutDataDto = checkout ? JSON.parse(checkout) : "";
  const isMobile = useBreakpointValue({ base: true, md: false });

  useEffect(() => {
    if (!checkout) return navigate("/");
    if (orderTotal === undefined) return navigate("/");

    setOrderTotal(
      checkoutItems?.orderTotal ? checkoutItems.orderTotal : undefined
    );
  }, []);

  useTimeout(() => removeCheckout(), 5000);

  return (
    <Stack minH="100vh" w="100%" position="relative">
      <Header />
      <Image
        src={
          isMobile
            ? checkoutItems.mobileBannerImageUrl
            : checkoutItems.desktopBannerImageUrl
        }
        maxH={{ base: "250px", md: "472px" }}
        mt={1}
      ></Image>

      <Box bgColor="secondary.500" width="100%" h={6} mt={2}></Box>
      <Container>
        <Box
          p="15px 45px"
          bgColor="neutral.0"
          borderRadius={6}
          border="solid 1px"
          borderColor="neutral.10"
          my={35}
        >
          <Grid
            w="100%"
            m="10px 0"
            templateColumns={{
              base: "repeat(1, 1fr)",
              md: "repeat(3, 1fr)",
              lg: "repeat(3, 1fr)",
            }}
            gap={8}
          >
            <GridItem colSpan={{ base: 3, md: 3, lg: 2 }}>
              <Description />
            </GridItem>
            <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
              <Stack justifyContent="center" alignItems="center" mt={10}>
                <PaymentCheckIcon />
              </Stack>
            </GridItem>
          </Grid>
          <Grid
            w="100%"
            templateColumns={{
              base: "repeat(1, 1fr)",
              md: "repeat(2, 1fr)",
              lg: "repeat(2, 1fr)",
            }}
            gap={{ base: 5, md: 100 }}
            mt={50}
          >
            <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
              <HStack
                justifyContent={{
                  base: "center",
                  md: "flex-end",
                }}
              >
                <Button
                  variant="ghost"
                  bgColor="neutral.0"
                  onClick={() => {
                    navigate("/");
                  }}
                  border="solid 1px"
                  borderColor="secondary.500"
                  w={{ base: "100%", md: "250px" }}
                  _hover={{
                    bgColor: "secondary.100",
                  }}
                  _active={{
                    bgColor: "secondary.300",
                  }}
                >
                  <Text
                    fontFamily="Inter"
                    fontStyle="normal"
                    fontWeight="600"
                    fontSize="16px"
                    lineHeight="20px"
                    color="secondary.500"
                  >
                    Página Inicial
                  </Text>
                </Button>
              </HStack>
            </GridItem>
            <GridItem colSpan={{ base: 3, md: 3, lg: 1 }}>
              <HStack
                justifyContent={{
                  base: "center",
                  md: "flex-start",
                }}
              >
                <Button
                  variant="ghost"
                  bgColor="secondary.500"
                  w={{ base: "100%", md: "250px" }}
                  _hover={{
                    bgColor: "secondary.300",
                  }}
                  _active={{
                    bgColor: "secondary.500",
                  }}
                  onClick={() => {
                    navigate("/pedidos");
                  }}
                >
                  <Text
                    fontFamily="Inter"
                    fontStyle="normal"
                    fontWeight="400"
                    fontSize="16px"
                    lineHeight="20px"
                    color="neutral.0"
                  >
                    Meus Pedidos
                  </Text>
                </Button>
              </HStack>
            </GridItem>
          </Grid>
        </Box>
      </Container>
      <Footer />
    </Stack>
  );
};
