import { QuantityCounter } from "@/components/quantityCounter";
import { Separator } from "@/components/separator";
import { useCheckout } from "@/hooks/useCheckout";
import { Box, Button, HStack, Stack, Text } from "@chakra-ui/react";
import { useNavigate, useParams } from "react-router-dom";
import { StoreEventDto } from "../home/types";
import { moneyFormat } from "@/utils/formatUtil";
import { useMemo, useState } from "react";
import { CheckoutDataDto } from "../checkout/types";

type Props = Pick<StoreEventDto, "products">;

export const BuyBox = ({ products }: Props) => {
  const { slug } = useParams();
  const { setCheckout, getCheckout } = useCheckout();
  const checkout = getCheckout();
  const checkoutItems: CheckoutDataDto = checkout
    ? JSON.parse(checkout)
    : undefined;

  const [productQuantity, setProductQuantity] = useState<{
    [key: string]: number;
  }>(
    products.reduce((acc: any, curr) => {
      if (!checkoutItems) {
        acc[curr.name] = 0;
        return acc;
      }

      const selectedProduct = Object.entries(checkoutItems.items).find(
        (item) => item[0] === curr.name
      );

      const quantity = selectedProduct ? selectedProduct[1] : 0;

      acc[curr.name] = quantity;
      return acc;
    }, {})
  );

  const navigate = useNavigate();

  const onCheckout = () => {
    setCheckout({ items: productQuantity });
    navigate(`/evento/${slug}/conferencia`);
  };

  const orderTotal = useMemo(() => {
    return Object.entries(productQuantity).reduce((acc, curr) => {
      const name = curr[0];
      const quantity = curr[1];
      const productValue = products.find(
        (product) => product.name === name
      )!.unitaryValue;

      acc += Number(productValue) * quantity;
      return acc;
    }, 0);
  }, [productQuantity]);

  return (
    <Box
      bgColor="neutral.0"
      w={{ base: "100%", md: "100%", lg: "400px" }}
      p="20px 15px"
      borderRadius={7}
      border="solid 1px"
      borderColor="neutral.10"
    >
      <Stack spacing={5}>
        {products.map((product) => (
          <HStack key={product.name} justifyContent="space-between">
            <Box>
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="600"
                fontSize={{ base: "17px", md: "18px" }}
                lineHeight="30px"
                color="neutral.50"
              >
                {product.name}
              </Text>
              <HStack>
                <Text
                  fontFamily="Inter"
                  fontStyle="normal"
                  fontWeight="600"
                  fontSize={{ base: "14px", md: "16px" }}
                  lineHeight="30px"
                  color="neutral.50"
                >
                  {moneyFormat(Number(product.unitaryValue))}
                </Text>
                <Text
                  fontFamily="Inter"
                  fontStyle="normal"
                  fontWeight="600"
                  fontSize={{ base: "10px", md: "12px" }}
                  lineHeight="30px"
                  color="neutral.50"
                >
                  un.
                </Text>
              </HStack>
            </Box>
            <QuantityCounter
              productName={product.name}
              productQuantity={productQuantity}
              setProductQuantity={setProductQuantity}
            />
          </HStack>
        ))}
      </Stack>
      <Separator m="15px 0" />
      <HStack justifyContent="space-between">
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="400"
          fontSize={{ base: "14px", md: "16px" }}
          lineHeight="20px"
          color="neutral.600"
        >
          Total do pedido
        </Text>
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="700"
          fontSize={{ base: "16px", md: "18px" }}
          lineHeight="20px"
          color="secondary.500"
        >
          {moneyFormat(orderTotal)}
        </Text>
      </HStack>
      <HStack justifyContent="flex-end" mt={2}>
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="400"
          fontSize={{ base: "10px", md: "13px" }}
          lineHeight="20px"
          color="neutral.600"
        >
          no pix ou cartão de crédito
        </Text>
      </HStack>
      <Stack spacing={4} p="35px 30px 5px 30px">
        <Button
          variant="ghost"
          bgColor="secondary.500"
          onClick={onCheckout}
          isDisabled={!orderTotal}
          _hover={{
            bgColor: "secondary.300",
          }}
          _active={{
            bgColor: "secondary.500",
          }}
        >
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="400"
            fontSize="16px"
            lineHeight="20px"
            color="neutral.0"
          >
            Finalizar compra
          </Text>
        </Button>
        <Button
          variant="ghost"
          bgColor="neutral.0"
          border="solid 1px"
          borderColor="secondary.500"
          onClick={() => navigate("/vendas")}
          _hover={{
            bgColor: "secondary.100",
          }}
          _active={{
            bgColor: "secondary.300",
          }}
        >
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="600"
            fontSize="16px"
            lineHeight="20px"
            color="secondary.500"
          >
            Vendas em meu nome
          </Text>
        </Button>
      </Stack>
    </Box>
  );
};
