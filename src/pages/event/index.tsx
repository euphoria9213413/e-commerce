import { Container } from "@/components/container";
import { Footer } from "@/components/footer";
import { Header } from "@/components/header";
import { Box, Image, Stack, useBreakpointValue } from "@chakra-ui/react";
import { ContentDesktop } from "./contentDesktop";
import { ContentMobile } from "./contentMobile";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useStoreEvent } from "@/hooks/useStore";
import { StoreEventDto } from "../home/types";
import { useCustomToast } from "@/hooks/useToast";
import LoadingOverlay from "@/components/overlay";

export const Event = () => {
  const { slug } = useParams();
  const { getStoreEventBySlug } = useStoreEvent();
  const { customToast } = useCustomToast();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [event, setEvent] = useState<StoreEventDto | undefined>();

  const navigate = useNavigate();
  const isMobile = useBreakpointValue({ base: true, md: false });
  const isTablet = useBreakpointValue({ base: true, md: true, lg: false });

  useEffect(() => {
    setIsLoading(true);

    getStoreEventBySlug(slug!)
      .then((response) => {
        setEvent(response.data);
      })
      .catch(() => {
        customToast("error", "Falha ao carregar evento");
        navigate("/");
      })
      .finally(() => setIsLoading(false));
  }, []);

  if (!event || isLoading) {
    return (
      <Stack minH="100vh" w="100%" position="relative">
        <LoadingOverlay />
        <Header />
        <Footer />
      </Stack>
    );
  }

  return (
    <Stack minH="100vh" w="100%" position="relative">
      <Header />
      <Image
        src={isMobile ? event.mobileMediaUrl : event.desktopMediaUrl}
        maxH={{ base: "250px", md: "472px" }}
        mt={1}
      ></Image>
      <Box bgColor="secondary.500" width="100%" h={6} mt={2}></Box>
      <Container>
        {isTablet ? (
          <ContentMobile event={event!} />
        ) : (
          <ContentDesktop event={event!} />
        )}
      </Container>
      <Footer />
    </Stack>
  );
};
