import { ArrowIcon } from "@/components/icons/arrow";
import {
  Accordion,
  AccordionButton,
  AccordionItem,
  AccordionPanel,
  Box,
  Flex,
  Text,
} from "@chakra-ui/react";
import ReactPlayer from "react-player";
import { StoreEventDto } from "../home/types";
import parse from "html-react-parser";

type Props = Pick<StoreEventDto, "description" | "videoUrl">;

export const Description = ({ description, videoUrl }: Props) => {
  return (
    <Accordion
      allowMultiple
      bgColor="neutral.0"
      borderRadius={7}
      w="100%"
      border="solid 1px"
      borderColor="neutral.10"
      mb={12}
      defaultIndex={[0]}
    >
      <AccordionItem>
        <AccordionButton
          p={{ base: "15px 25px 10px 25px", md: "30px 50px 20px 50px" }}
        >
          <Box as="span" flex="1" textAlign="left">
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="600"
              fontSize={{ base: "16px", md: "18px" }}
              lineHeight="30px"
              color="secondary.500"
            >
              Descrição
            </Text>
          </Box>
          <ArrowIcon />
        </AccordionButton>
        <AccordionPanel
          p={{ base: "5px 25px 15px 25px", md: "10px 50px 30px 50px" }}
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="600"
          lineHeight="20px"
          color="neutral.600"
          fontSize={{ base: "13px", md: "15px" }}
        >
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="400"
            fontSize={{ base: "12px", md: "14px" }}
            lineHeight="20px"
            color="neutral.600"
          >
            {parse(description || "")}
          </Text>
          <Flex w="100%" m="50px 0" justifyContent="center">
            <ReactPlayer url={videoUrl} />
          </Flex>
        </AccordionPanel>
      </AccordionItem>
    </Accordion>
  );
};
