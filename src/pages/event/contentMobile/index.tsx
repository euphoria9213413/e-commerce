import { Stack } from "@chakra-ui/react";
import { Information } from "../information";
import { Description } from "../description";
import { BuyBox } from "../buyBox";
import { StoreEventDto } from "@/pages/home/types";

type Props = {
  event: StoreEventDto;
};

export const ContentMobile = ({ event }: Props) => {
  return (
    <Stack w="100%" spacing={5} mt={3}>
      <BuyBox products={event.products} />
      <Information {...event} />
      <Description {...event} />
    </Stack>
  );
};
