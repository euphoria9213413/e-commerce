import { ArrowIcon } from "@/components/icons/arrow";
import {
  Accordion,
  AccordionButton,
  AccordionItem,
  AccordionPanel,
  Box,
  HStack,
  Text,
} from "@chakra-ui/react";
import { StoreEventDto } from "../home/types";
import { getDate, getHour } from "@/utils/dateUtil";

type Props = Pick<
  StoreEventDto,
  "raffleDate" | "initialDate" | "finalDate" | "raffleUrl" | "regulationUrl"
>;

export const Information = ({
  raffleDate,
  initialDate,
  finalDate,
  raffleUrl,
  regulationUrl,
}: Props) => {
  return (
    <Accordion
      allowMultiple
      bgColor="neutral.0"
      borderRadius={7}
      w="100%"
      border="solid 1px"
      borderColor="neutral.10"
      defaultIndex={[0]}
    >
      <AccordionItem>
        <AccordionButton
          p={{ base: "15px 25px 10px 25px", md: "30px 50px 20px 50px" }}
        >
          <Box as="span" flex="1" textAlign="left">
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="600"
              fontSize={{ base: "16px", md: "18px" }}
              lineHeight="30px"
              color="secondary.500"
            >
              Informações Importantes
            </Text>
          </Box>
          <ArrowIcon />
        </AccordionButton>
        <AccordionPanel
          p={{ base: "5px 25px 15px 25px", md: "10px 50px 30px 50px" }}
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="600"
          lineHeight="20px"
          color="neutral.600"
          fontSize={{ base: "13px", md: "15px" }}
        >
          <HStack
            w="100%"
            h="47px"
            bgColor="secondary.50"
            borderRadius={5}
            p={{ base: "0 15px", md: "0 30px" }}
          >
            <Text
              w={{ base: "35%", md: "25%" }}
              wordBreak="break-all"
              whiteSpace="pre-wrap"
            >
              Data de início
            </Text>
            <Text
              w={{ base: "65%", md: "75%" }}
              wordBreak="break-all"
              whiteSpace="pre-wrap"
            >{`${getDate(initialDate)} ${getHour(initialDate)}`}</Text>
          </HStack>
          <HStack
            w="100%"
            h="47px"
            bgColor="neutral.0"
            borderRadius={5}
            p={{ base: "0 15px", md: "0 30px" }}
          >
            <Text
              w={{ base: "35%", md: "25%" }}
              wordBreak="break-all"
              whiteSpace="pre-wrap"
            >
              Data de fim
            </Text>
            <Text
              w={{ base: "65%", md: "75%" }}
              wordBreak="break-all"
              whiteSpace="pre-wrap"
            >{`${getDate(finalDate)} ${getHour(finalDate)}`}</Text>
          </HStack>
          <HStack
            w="100%"
            h="47px"
            bgColor="secondary.50"
            borderRadius={5}
            p={{ base: "0 15px", md: "0 30px" }}
          >
            <Text
              w={{ base: "35%", md: "25%" }}
              wordBreak="break-all"
              whiteSpace="pre-wrap"
            >
              Data de sorteio
            </Text>
            <Text
              w={{ base: "65%", md: "75%" }}
              wordBreak="break-all"
              whiteSpace="pre-wrap"
            >{`${raffleDate ? getDate(raffleDate) : ""} ${
              raffleDate ? getHour(raffleDate) : ""
            }`}</Text>
          </HStack>
          <HStack
            w="100%"
            h="47px"
            bgColor="neutral.0"
            borderRadius={5}
            p={{ base: "0 15px", md: "0 30px" }}
          >
            <Text
              w={{ base: "35%", md: "25%" }}
              wordBreak="break-all"
              whiteSpace="pre-wrap"
            >
              Link do sorteio
            </Text>
            <Text
              w={{ base: "65%", md: "75%" }}
              wordBreak="break-all"
              whiteSpace="pre-wrap"
            >
              <a href={raffleUrl} target="_blank" rel="noreferrer">
                {raffleUrl}
              </a>
            </Text>
          </HStack>
          <HStack
            w="100%"
            h="47px"
            bgColor="secondary.50"
            borderRadius={5}
            p={{ base: "0 15px", md: "0 30px" }}
          >
            <Text
              w={{ base: "35%", md: "25%" }}
              wordBreak="break-all"
              whiteSpace="pre-wrap"
              color="neutral.600"
              fontWeight="600"
            >
              Regulamento
            </Text>
            <Text
              w={{ base: "65%", md: "75%" }}
              wordBreak="break-all"
              whiteSpace="pre-wrap"
              fontWeight="700"
              color="blue.700"
              textDecoration="underline"
              cursor="pointer"
            >
              <a href={regulationUrl} target="_blank" rel="noreferrer">
                {regulationUrl}
              </a>
            </Text>
          </HStack>
        </AccordionPanel>
      </AccordionItem>
    </Accordion>
  );
};
