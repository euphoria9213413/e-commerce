import { HStack, Stack } from "@chakra-ui/react";
import { Information } from "../information";
import { Description } from "../description";
import { BuyBox } from "../buyBox";
import { StoreEventDto } from "@/pages/home/types";

type Props = {
  event: StoreEventDto;
};

export const ContentDesktop = ({ event }: Props) => {
  return (
    <HStack w="100%" alignItems="flex-start" spacing="50px" mt={5}>
      <Stack w="100%" spacing={10}>
        <Information {...event} />
        <Description {...event} />
      </Stack>
      <BuyBox products={event.products} />
    </HStack>
  );
};
