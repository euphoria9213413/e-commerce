import { SaleIndexDto } from "@/pages/sales/types";

export type OrderIndexFilterDto = {
  buyerNameOrBuyerEmailOrGraduateName: string;
};

export type Props = {
  page: number;
  lastPage: number;
  setPage: (value: number) => void;
  onResentEmail: (id: number) => void;
  onPixCopy: (order: SaleIndexDto) => void;
};
