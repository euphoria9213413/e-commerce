import {
  Stack,
  Table,
  TableContainer,
  Tbody,
  Td,
  Tfoot,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import * as Font from "@/components/texts";
import { MenuActionButton } from "@/components/menuActionButton";
import { useState } from "react";
import { MenuPopUp } from "@/components/menuPopUp";
import { Pagination } from "@/components/pagination";
import * as T from "@/components/table";
import { Props } from "./types";
import { SaleIndexDto } from "../sales/types";
import { moneyFormat } from "@/utils/formatUtil";
import { PaymentStatusEnum, PaymentTypeEnum } from "./enums";

type DesktopProps = {
  orders: SaleIndexDto[];
} & Props;

export const DesktopContent = ({
  orders,
  page,
  lastPage,
  setPage,
  onResentEmail,
  onPixCopy,
}: DesktopProps) => {
  const [showMenuPopUp, setShowMenuPopUp] = useState<undefined | number>();

  const menuOptions = (order: SaleIndexDto) => {
    if (
      order.paymentType === PaymentTypeEnum.pix &&
      PaymentStatusEnum[order.paymentStatus] === PaymentStatusEnum.ativa
    ) {
      return [
        {
          title: "Reenviar e-mail",
          onClick: () => onResentEmail(order.id),
        },
        {
          title: "Copiar código PIX",
          onClick: () => onPixCopy(order),
        },
      ];
    }

    return [
      {
        title: "Reenviar e-mail",
        onClick: () => onResentEmail(order.id),
      },
    ];
  };

  return (
    <Stack h="100%" w="100%" p={5}>
      <TableContainer>
        <Table w="100%">
          <Thead>
            <Tr>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Data e hora</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Número da sorte</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Nome loja</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Nome formando</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Nome comprador</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>E-mail comprador</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Telefone comprador</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Produto</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Valor</Font.TableTheadText>
              </Th>
              <Th
                textAlign="center"
                wordBreak="break-all"
                whiteSpace="pre-wrap"
              >
                <Font.TableTheadText>Status</Font.TableTheadText>
              </Th>
              <Th></Th>
            </Tr>
          </Thead>
          <Tbody>
            {orders.map((order) => (
              <T.Body key={order.id}>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{`${order.date} ${order.hour}`}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{order.luckyNumber}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{order.storeName}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{order.graduateName}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{order.buyerName}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{order.buyerEmail}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{order.buyerContact}</Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>{order.productName}</Font.TableBodyText>
                </Td>
                <Td textAlign="center">
                  <Font.TableBodyText>
                    {moneyFormat(Number(order.price))}
                  </Font.TableBodyText>
                </Td>
                <Td
                  textAlign="center"
                  wordBreak="break-all"
                  whiteSpace="pre-wrap"
                >
                  <Font.TableBodyText>
                    {PaymentStatusEnum[order.paymentStatus]}
                  </Font.TableBodyText>
                </Td>
                <Td>
                  <MenuActionButton
                    color="#2F2F2F"
                    onClick={() => setShowMenuPopUp(order.id)}
                    _hover={{
                      bgColor: "neutral.200",
                    }}
                    _active={{
                      bgColor: "neutral.300",
                    }}
                  />
                </Td>
                {showMenuPopUp === order.id && (
                  <MenuPopUp
                    top="-4px"
                    right="36px"
                    options={menuOptions(order)}
                    setShowMenuPopUp={setShowMenuPopUp}
                  />
                )}
              </T.Body>
            ))}
          </Tbody>
          <Tfoot w="100%" h="15px"></Tfoot>
        </Table>
        <Pagination page={page} lastPage={lastPage} onClick={setPage} />
      </TableContainer>
    </Stack>
  );
};
