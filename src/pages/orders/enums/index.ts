export enum PaymentStatusEnum {
  devolvido = "Estornado",
  ativa = "Pendente",
  concluida = "Aprovado",
  removida_pelo_usuario_recebedor = "Removida pelo recebedor",
  removida_pelo_app = "Removida pelo app",
  cancelado = "Cancelado",
}

export type PaymentStatusType = keyof typeof PaymentStatusEnum;

export enum PaymentTypeEnum {
  credit_card = "credit_card",
  pix = "pix",
}
