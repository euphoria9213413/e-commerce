import {
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Text,
} from "@chakra-ui/react";
import { moneyFormat } from "@/utils/formatUtil";
import { QrCodeBox } from "../payment/qrCodeBox";

type Props = {
  isOpen: boolean;
  onClose: () => void;
  price: number;
  pixCopyPasteValue?: string;
  expirationDate?: string;
  expirationHour?: string;
};

export const PixModal = ({
  isOpen,
  onClose,
  price,
  pixCopyPasteValue,
  expirationDate,
  expirationHour,
}: Props) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <ModalOverlay />
      <ModalContent
        bgColor="neutral.100"
        w={{ base: "250px", md: "400px" }}
        pb={5}
      >
        <ModalHeader>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="700"
            fontSize="24px"
            lineHeight="32px"
            color="neutral.600"
            textAlign="center"
          >
            Código PIX
          </Text>
        </ModalHeader>
        <ModalBody>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="500"
            fontSize="14px"
            lineHeight="22px"
            color="neutral.600"
            textAlign="center"
          >
            Total do Pedido: {moneyFormat(price)}
          </Text>
          <QrCodeBox
            pixCopyPasteValue={pixCopyPasteValue}
            expirationDate={expirationDate}
            expirationHour={expirationHour}
          />
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};
