import {
  Input,
  InputGroup,
  InputLeftElement,
  Stack,
  useBreakpointValue,
  useDisclosure,
} from "@chakra-ui/react";
import { DesktopContent } from "./desktopContent";
import { Header } from "@/components/header";
import { FilterBar } from "@/components/FilterBar";
import { SearchIcon } from "@/components/icons/search";
import { OrderIndexFilterDto } from "./types";
import { useCallback, useEffect, useState } from "react";
import { Footer } from "@/components/footer";
import LoadingOverlay from "@/components/overlay";
import { useCustomToast } from "@/hooks/useToast";
import { SaleIndexDto } from "../sales/types";
import { useSale } from "@/hooks/useSale";
import { PixModal } from "./pixModal";
import { MobileContent } from "./mobileContent";

const initialFilter: OrderIndexFilterDto = {
  buyerNameOrBuyerEmailOrGraduateName: "",
};

/* eslint-disable react/no-children-prop */
export const Orders = () => {
  const { getOrders, resendEmail } = useSale();
  const [filter, setFilter] = useState<OrderIndexFilterDto>(initialFilter);
  const [typingTimer, setTypingTimer] = useState<NodeJS.Timeout | undefined>();
  const [stopedTyping, setStopedTyping] = useState(false);
  const { customToast } = useCustomToast();
  const [page, setPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [orders, setOrders] = useState<SaleIndexDto[]>([]);
  const [totalOrders, setTotalOrders] = useState<number>(0);
  const [lastPage, setLastPage] = useState<number>(1);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [selectedOrder, setSelectedOrder] = useState<SaleIndexDto>();

  const isTablet = useBreakpointValue({ base: true, md: true, lg: false });

  const handleInputChange = () => {
    clearTimeout(typingTimer);

    const newTypingTimer = setTimeout(() => {
      setStopedTyping(true);
    }, 500);

    setTypingTimer(newTypingTimer);
  };

  const callbackOrders = useCallback(() => {
    setIsLoading(true);

    getOrders({ page, ...filter })
      .then((response) => {
        setOrders(response.data.data);
        setTotalOrders(response.data.total);
        setLastPage(response.data.lastPage);
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  }, [page, filter]);

  useEffect(() => {
    callbackOrders();
  }, [page]);

  useEffect(() => {
    if (stopedTyping) {
      if (page === 1) {
        callbackOrders();
      } else {
        setPage(1);
      }
      setStopedTyping(false);
    }
  }, [page, filter, stopedTyping]);

  const onResentEmail = (saleId: number) => {
    setIsLoading(true);

    resendEmail(saleId)
      .then(() => customToast("success", "E-mail reenviado com sucesso"))
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  const onPixCopy = (order: SaleIndexDto) => {
    setSelectedOrder(order);

    onOpen();
  };

  const filters = [
    {
      field: (
        <InputGroup alignItems="center" key={1}>
          <InputLeftElement
            pointerEvents="none"
            children={<SearchIcon />}
            top=""
            left={0}
          />
          <Input
            type="text"
            placeholder="E-mail, nome do comprador ou formando"
            _placeholder={{
              color: "neutral.50",
            }}
            w="100%"
            h={9}
            borderRadius={4}
            border="1px solid #EAEAEA"
            bgColor="neutral.0"
            onChange={(event) => {
              setFilter({
                ...filter,
                buyerNameOrBuyerEmailOrGraduateName: event.target.value,
              });
              handleInputChange();
              setPage(1);
            }}
          />
        </InputGroup>
      ),
    },
  ];

  return (
    <Stack
      h={orders.length < 10 ? "100vh" : "100%"}
      w="100%"
      position="relative"
      gap={0}
    >
      <Header />
      <FilterBar
        title={`Meus pedidos (${totalOrders})`}
        subTitle="Lista de formandos vendedores"
        filters={filters}
      />
      {isLoading && <LoadingOverlay />}
      {isTablet ? (
        <MobileContent
          orders={orders}
          page={page}
          setPage={setPage}
          lastPage={lastPage}
          onResentEmail={onResentEmail}
          onPixCopy={onPixCopy}
        />
      ) : (
        <DesktopContent
          onResentEmail={onResentEmail}
          orders={orders}
          page={page}
          setPage={setPage}
          lastPage={lastPage}
          onPixCopy={onPixCopy}
        />
      )}
      {!isTablet && <Footer />}
      {selectedOrder && (
        <PixModal
          key={selectedOrder.id}
          isOpen={isOpen}
          onClose={onClose}
          price={Number(selectedOrder.price)}
          pixCopyPasteValue={selectedOrder.paymentCopyPaste}
          expirationDate={selectedOrder.expirationDate}
          expirationHour={selectedOrder.expirationHour}
        />
      )}
    </Stack>
  );
};
