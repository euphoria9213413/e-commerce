import { Box, HStack, Stack, Text, VStack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { Footer } from "@/components/footer";
import InfiniteScroll from "react-infinite-scroll-component";
import { SaleIndexDto } from "../sales/types";
import { Props } from "./types";
import { moneyFormat } from "@/utils/formatUtil";
import { PaymentStatusEnum, PaymentTypeEnum } from "./enums";

type MobileProps = { orders: SaleIndexDto[] } & Props;

export const MobileContent = ({
  orders,
  page,
  lastPage,
  setPage,
  onResentEmail,
  onPixCopy,
}: MobileProps) => {
  const [mobileOrders, setMobileOrders] = useState<SaleIndexDto[]>(orders);

  useEffect(() => {
    if (page > 1) return setMobileOrders([...mobileOrders, ...orders]);

    setMobileOrders(orders);
  }, [orders]);

  const heiht = mobileOrders.length > 1 ? {} : { h: "100%" };

  const isPixPayment = (order: SaleIndexDto) =>
    order.paymentType === PaymentTypeEnum.pix &&
    PaymentStatusEnum[order.paymentStatus] === PaymentStatusEnum.ativa;

  return (
    <VStack
      bgColor="neutral.200"
      {...heiht}
      w="100%"
      flexDirection="column-reverse"
    >
      <Footer />
      <VStack h="100%" w="100%" p="0 10px" my={4}>
        <InfiniteScroll
          style={{ backgroundColor: "#EAEAEA" }}
          dataLength={mobileOrders.length}
          next={() => {
            setPage(page + 1);
          }}
          hasMore={page < lastPage}
          loader={<></>}
        >
          <Stack spacing={3} w="100%">
            {mobileOrders.map((order) => (
              <Box
                key={order.id}
                w={{ base: "360px", md: "500px" }}
                bgColor="neutral.0"
                pt={3}
                borderRadius={4}
              >
                <Stack spacing={5}>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Data e hora
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {`${order.date} ${order.hour}`}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Número da sorte
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {order.luckyNumber}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Nome loja
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {order.storeName}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Nome formando
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {order.graduateName}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Nome comprador
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {order.buyerName}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      E-mail comprador
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {order.buyerEmail}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Telefone comprador
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {order.buyerContact}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Produto
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {order.productName}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Valor
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {moneyFormat(Number(order.price))}
                    </Text>
                  </VStack>
                  <VStack justifyContent="center">
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="900"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                    >
                      Status
                    </Text>
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="400"
                      fontSize={{ base: "14px", md: "16px" }}
                      lineHeight="14px"
                      color="neutral.500"
                      display="inline-table"
                      whiteSpace="pre-wrap"
                    >
                      {PaymentStatusEnum[order.paymentStatus]}
                    </Text>
                  </VStack>
                </Stack>
                <HStack
                  justifyContent={`${
                    isPixPayment(order) ? "space-between" : "flex-end"
                  }`}
                  mr={3}
                  mt={3}
                >
                  {isPixPayment(order) && (
                    <Text
                      fontFamily="Inter"
                      fontStyle="normal"
                      fontWeight="700"
                      fontSize={{ base: "12px", md: "14px" }}
                      lineHeight="18px"
                      color="blue.800"
                      textDecoration="underline"
                      onClick={() => onPixCopy(order)}
                    >
                      Copiar código Pix
                    </Text>
                  )}
                  <Text
                    fontFamily="Inter"
                    fontStyle="normal"
                    fontWeight="700"
                    fontSize={{ base: "12px", md: "14px" }}
                    lineHeight="18px"
                    color="blue.800"
                    textDecoration="underline"
                    onClick={() => onResentEmail(order.id)}
                  >
                    reenviar e-mail
                  </Text>
                </HStack>
              </Box>
            ))}
          </Stack>
        </InfiniteScroll>
      </VStack>
    </VStack>
  );
};
