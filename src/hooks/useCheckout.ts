import { api } from "@/configs/axiosInstance";
import {
  CheckoutDataDto,
  CheckoutFormDto,
  GraduateIndexPaginatedDto,
  GraduateIndexQueryDto,
  SalePixCopyPaste,
} from "@/pages/checkout/types";
import { LocalStorageEnum } from "@/utils/localStorage";

export const useCheckout = () => {
  const setCheckout = (data: CheckoutDataDto) =>
    localStorage.setItem(LocalStorageEnum.CHECKOUT, JSON.stringify(data));

  const getCheckout = () => localStorage.getItem(LocalStorageEnum.CHECKOUT);

  const removeCheckout = () =>
    localStorage.removeItem(LocalStorageEnum.CHECKOUT);

  const getGraduates = ({ storeId, name }: GraduateIndexQueryDto) => {
    let params = `page=${1}&perPage=${100}&situation=ativos`;

    if (name) {
      params = `${params}&name=${name}`;
    }

    return api.get<GraduateIndexPaginatedDto>(
      `/graduate/${storeId}/index?${params}`
    );
  };

  const storeSale = (data: CheckoutFormDto) => {
    return api.post<SalePixCopyPaste>(`/sale/create`, data);
  };

  return { getCheckout, setCheckout, removeCheckout, getGraduates, storeSale };
};
