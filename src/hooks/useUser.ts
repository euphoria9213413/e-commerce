import { api } from "@/configs/axiosInstance";
import { LoginAuthenticated } from "@/pages/login/types";
import {
  UserDataDto,
  UserEmailUpdateDto,
  UserPasswordUpdateDto,
  UserStoreDto,
} from "@/pages/profile/types";

export const useUser = () => {
  const userPasswordUpdate = (id: number, data: UserPasswordUpdateDto) => {
    return api.patch<LoginAuthenticated>(
      `/account/password-update/by-token`,
      data
    );
  };

  const userEmailUpdate = (id: number, data: UserEmailUpdateDto) => {
    return api.patch(`/account/email-update/by-token`, data);
  };

  const getUser = () => {
    return api.get<UserDataDto>(`account/by-token`);
  };

  const userUpdate = (data: UserStoreDto) => {
    return api.put(`account/by-token`, data);
  };

  return {
    userPasswordUpdate,
    userEmailUpdate,
    getUser,
    userUpdate,
  };
};
