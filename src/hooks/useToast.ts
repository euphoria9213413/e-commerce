import { AlertStatus, useToast } from "@chakra-ui/react";

export const useCustomToast = () => {
  const toast = useToast();

  const customToast = (type: AlertStatus, description: string) => {
    return toast({
      description: description,
      status: type,
      position: "top-right",
      duration: 5000,
      isClosable: true,
    });
  };

  return { customToast };
};
