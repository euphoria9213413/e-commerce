import { api } from "@/configs/axiosInstance";
import { env } from "@/configs/env";
import {
  OrderIndexQueryDto,
  SaleIndexPaginatedDto,
  SaleIndexQueryDto,
} from "@/pages/sales/types";

export const useSale = () => {
  const getSales = ({ graduateCpf, storeName, page }: SaleIndexQueryDto) => {
    let params = `page=${page}&perPage=${env.per_page}`;

    if (graduateCpf) {
      params = `${params}&graduateCpf=${graduateCpf}`;
    }

    if (storeName) {
      params = `${params}&storeName=${storeName}`;
    }

    return api.get<SaleIndexPaginatedDto>(`/sale/index/mine?${params}`);
  };

  const getOrders = ({
    buyerNameOrBuyerEmailOrGraduateName,
    page,
  }: OrderIndexQueryDto) => {
    let params = `page=${page}&perPage=${env.per_page}`;

    if (buyerNameOrBuyerEmailOrGraduateName) {
      params = `${params}&buyerNameOrBuyerEmailOrGraduateName=${buyerNameOrBuyerEmailOrGraduateName}`;
    }

    return api.get<SaleIndexPaginatedDto>(
      `/sale/index/order/by-token?${params}`
    );
  };

  const resendEmail = (id: number): Promise<void> => {
    return api.get(`sale/${id}/resend-email`);
  };

  return { getSales, getOrders, resendEmail };
};
