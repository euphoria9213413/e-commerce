import {
  LoginAuthenticated,
  LoginSubmitDto,
  StoreAccountDto,
} from "@/pages/login/types";
import { UpdateAccountPasswordDto } from "@/pages/newPassword/types/newPassword";
import { api } from "@/configs/axiosInstance";
import { LocalStorageEnum } from "@/utils/localStorage";
import { RecoverPasswordDto } from "@/pages/recoverPassword/types";

export const useLogin = () => {
  const isAuthenticated = !!localStorage.getItem(LocalStorageEnum.ACCESS_TOKEN);

  const doLogin = (data: LoginSubmitDto) => {
    return api.post<LoginAuthenticated>("/account/login", data);
  };

  const setAuthentication = (authentication: LoginAuthenticated) =>
    localStorage.setItem(
      LocalStorageEnum.ACCESS_TOKEN,
      JSON.stringify(authentication)
    );

  const removeAuthentication = () =>
    localStorage.removeItem(LocalStorageEnum.ACCESS_TOKEN);

  const getAccount = (): LoginAuthenticated | undefined => {
    const authentication = localStorage.getItem(LocalStorageEnum.ACCESS_TOKEN);

    if (!authentication) return;

    return JSON.parse(authentication);
  };

  const doStore = (data: StoreAccountDto) => {
    return api.post("/account/create", data);
  };

  const doSendPasswordRecoverEmail = (
    data: RecoverPasswordDto
  ): Promise<void> => {
    return api.post("/password-recover", data);
  };

  const doNewPassword = (
    data: UpdateAccountPasswordDto,
    token: string
  ): Promise<void> => {
    return api.post(`/password-recover/${token}`, data);
  };

  return {
    isAuthenticated,
    doLogin,
    setAuthentication,
    removeAuthentication,
    getAccount,
    doStore,
    doSendPasswordRecoverEmail,
    doNewPassword,
  };
};
