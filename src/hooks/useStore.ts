import { api } from "@/configs/axiosInstance";
import { StoreEventDto, StoreEventFilterDto } from "@/pages/home/types";

export const useStoreEvent = () => {
  const getStoreEvents = ({ name }: StoreEventFilterDto) => {
    let path = "/store/events";

    if (name) {
      path = `${path}?name=${name}`;
    }

    return api.get<StoreEventDto[]>(path);
  };

  const getStoreEventBySlug = (slug: string) => {
    return api.get<StoreEventDto>(`/store/event/${slug}`);
  };

  return {
    getStoreEvents,
    getStoreEventBySlug,
  };
};
