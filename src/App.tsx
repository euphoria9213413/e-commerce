import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { theme } from "./theme";
import { AppRoutes } from "./routes";
import { BrowserRouter } from "react-router-dom";
import { GlobalFonts } from "./fonts";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "./styles.css";

function App() {
  return (
    <ChakraProvider theme={extendTheme(theme)}>
      <BrowserRouter>
        <GlobalFonts />
        <div className="container">
          <AppRoutes />
        </div>
      </BrowserRouter>
    </ChakraProvider>
  );
}

export default App;
