import { Route, Routes } from "react-router";
import { Navigate, useLocation } from "react-router-dom";
import { Home } from "./pages/home";
import { Login } from "./pages/login";
import { NewAccount } from "./pages/newAccount";
import { NewPassword } from "./pages/newPassword";
import { Event } from "./pages/event";
import { Checkout } from "./pages/checkout";
import { Payment } from "./pages/payment";
import { Profile } from "./pages/profile";
import { Orders } from "./pages/orders";
import { Sales } from "./pages/sales";
import { RecoverPassword } from "./pages/recoverPassword";
import { useCustomToast } from "./hooks/useToast";
import { useLogin } from "./hooks/useLogin";
import { PaymentAccept } from "./pages/paymentAccept";

export const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" Component={Home} />
      <Route path="/login" Component={Login} />
      <Route path="/criar-conta" Component={NewAccount} />
      <Route path="/recuperar-senha" Component={RecoverPassword} />
      <Route path="/alterar-senha/:token" Component={NewPassword} />
      <Route path="/perfil" Component={Profile} />
      <Route path="/evento/:slug" Component={Event} />
      <Route
        path="/evento/:slug/conferencia"
        element={
          <ProtectedRoute>
            <Checkout />
          </ProtectedRoute>
        }
      />
      <Route path="/pagamento" Component={Payment} />
      <Route path="/pagamento-aprovado" Component={PaymentAccept} />
      <Route path="/pedidos" Component={Orders} />
      <Route path="/vendas" Component={Sales} />
    </Routes>
  );
};

const ProtectedRoute = ({ children }: any) => {
  const { isAuthenticated } = useLogin();
  const { customToast } = useCustomToast();
  const location = useLocation();

  if (!isAuthenticated) {
    customToast("warning", "É necessário acessar sua conta para prosseguir.");
    return <Navigate to="/login" state={{ from: location.pathname }} replace />;
  }

  return children;
};
